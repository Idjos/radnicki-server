<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('lastname');
            $table->integer('number');
            $table->date('birthday');
            $table->text('desc');
            $table->string('place_of_birth');
            $table->enum('position', ['ГОЛМАН', 'ОДБРАНА', 'СРЕДИНА', 'НАПАД']);
            $table->timestamps();
            $table->string('path')->nullable();
            $table->string('country');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
