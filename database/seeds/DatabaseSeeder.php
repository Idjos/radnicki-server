<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ResultsTableSeeder::class);
        $this->call(TablesTableSeeder::class);
//        $this->call(EventsTableSeeder::class);
//        $this->call(LatestNewsTableSeeder::class);
//        $this->call(CommentsTableSeeder::class);
//        $this->call(MembersTableSeeder::class);
//        $this->call(ExpertsTableSeeder::class);
//        $this->call(SecondTeamsTableSeeder::class);
    }
}
