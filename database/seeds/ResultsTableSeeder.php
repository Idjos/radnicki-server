<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ResultsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('results')->insert([
            'home_club' => 'Раднички Ниш',
            'away_club' => 'Црвена Звезда',
            'home_go' => null,
            'away_go' => null,
            'date' => \Carbon\Carbon::now(),
            'type' => 0,
        ]);

        DB::table('results')->insert([
            'home_club' => 'Раднички Ниш',
            'away_club' => 'Партизан',
            'home_go' => 2,
            'away_go' => 1,
            'date' => \Carbon\Carbon::now(),
            'type' => 1,
        ]);
    }
}
