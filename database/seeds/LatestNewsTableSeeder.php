<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LatestNewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $dummy_status = ['НАЈВАЖНИЈЕ', 'КОНФЕРЕНЦИЈА', 'ИЗВЕШТАЈ'];
        $dummy_status2 = ['FIRST-TEAM', 'OTHER'];

        for($i = 0; $i <= 11; $i++) {
            DB::table('latest_news')->insert([
                'name' => $faker->name(),
                'desc' => $faker->text(),
                'type' => $dummy_status[rand(0,2)],
                'created_at' => \Carbon\Carbon::now(),
                'path' => 'file',
                'category' => $dummy_status2[rand(0,1)],
            ]);
        }
    }
}
