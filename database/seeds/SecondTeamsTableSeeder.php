<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SecondTeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i = 0; $i <= 10; $i++) {
            DB::table('second_teams')->insert([
                'name' => $faker->name(),
                'players' => $faker->text($maxNbChars = 20),
                'matches' => $faker->text($maxNbChars = 20),
            ]);
        }
    }
}
