<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExpertsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $dummy_status = ['FIRST-TEAM', 'OTHER'];
        for($i = 0; $i <= 10; $i++) {
            DB::table('experts')->insert([
                'name' => $faker->name(),
                'lastname' => $faker->lastName(),
                'position' => $faker->text($maxNbChars = 20),
                'created_at' => \Carbon\Carbon::now(),
                'path' => 'file',
                'category' => $dummy_status[rand(0,1)],
            ]);
        }
    }
}
