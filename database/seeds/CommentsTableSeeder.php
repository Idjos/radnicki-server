<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i = 0; $i <= 100; $i++) {
            DB::table('comments')->insert([
                'name' => $faker->name(),
                'desc' => $faker->text(),
                'email' => $faker->email(),
                'latest_new_id' => rand(1,10),
                'created_at' => \Carbon\Carbon::now(),
            ]);
        }
    }
}
