<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MembersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $dummy_status = ['ГОЛМАН', 'ОДБРАНА', 'СРЕДИНА', 'НАПАД'];
        for($i = 0; $i <= 10; $i++) {
            DB::table('members')->insert([
                'name' => $faker->name(),
                'lastname' => $faker->lastName(),
                'desc' => $faker->text(),
                'number' => rand(1,20),
                'birthday' => \Carbon\Carbon::now(),
                'place_of_birth' => $faker->text($maxNbChars = 20),
                'position' => $dummy_status[rand(0,3)],
                'created_at' => \Carbon\Carbon::now(),
                'path' => 'file',
            ]);
        }
    }
}
