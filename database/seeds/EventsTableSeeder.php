<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i = 0; $i <= 10; $i++) {
            DB::table('events')->insert([
                'name' => $faker->name(),
                'desc' => $faker->text($maxNbChars = 20),
                'date' => $faker->date(),
            ]);
        }
    }
}
