<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TablesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tables')->insert([
            'name' => 'Раднички Ниш',
            'played_games' => 0,
            'points' => 0,
        ]);

        DB::table('tables')->insert([
            'name' => 'Чукарички',
            'played_games' => 0,
            'points' => 0,
        ]);

        DB::table('tables')->insert([
            'name' => 'Црвена Звезда',
            'played_games' => 0,
            'points' => 0,
        ]);

        DB::table('tables')->insert([
            'name' => 'Партизан',
            'played_games' => 0,
            'points' => 0,
        ]);

        DB::table('tables')->insert([
            'name' => 'Младост',
            'played_games' => 0,
            'points' => 0,
        ]);

        DB::table('tables')->insert([
            'name' => 'Напредак',
            'played_games' => 0,
            'points' => 0,
        ]);

        DB::table('tables')->insert([
            'name' => 'Војводина',
            'played_games' => 0,
            'points' => 0,
        ]);

        DB::table('tables')->insert([
            'name' => 'Пролетер',
            'played_games' => 0,
            'points' => 0,
        ]);

        DB::table('tables')->insert([
            'name' => 'Мачва',
            'played_games' => 0,
            'points' => 0,
        ]);

        DB::table('tables')->insert([
            'name' => 'Рад',
            'played_games' => 0,
            'points' => 0,
        ]);

        DB::table('tables')->insert([
            'name' => 'Бачка',
            'played_games' => 0,
            'points' => 0,
        ]);

        DB::table('tables')->insert([
            'name' => 'ФК Вождовац',
            'played_games' => 0,
            'points' => 0,
        ]);

        DB::table('tables')->insert([
            'name' => 'Земун',
            'played_games' => 0,
            'points' => 0,
        ]);

        DB::table('tables')->insert([
            'name' => 'Радник',
            'played_games' => 0,
            'points' => 0,
        ]);

        DB::table('tables')->insert([
            'name' => 'Спартак Суботица',
            'played_games' => 0,
            'points' => 0,
        ]);

        DB::table('tables')->insert([
            'name' => 'Динамо Врање',
            'played_games' => 0,
            'points' => 0,
        ]);
    }
}
