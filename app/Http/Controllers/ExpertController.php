<?php

namespace App\Http\Controllers;

use App\Expert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Helpers\GeneralHelper;

class ExpertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $experts = Expert::where('category', 'FIRST-TEAM')->get();
        return view('admin/expert-staff')->with('experts', $experts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $expert = new Expert();
            $expert->fill($request->all());
            if ($request->hasFile('avatar')) {
                $helper = new GeneralHelper;
                $file = $helper->uploadFile($request->file('avatar'), 'experts');
                $expert['path'] = $file;
            }
            if ($expert->save()) {
                return back();
            };
            return back();
        } catch (\Exception $e) {

            return redirect('/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $expert = Expert::find($id);
        return view('/single-expert')->with('expert', $expert);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $expert = Expert::find($id);
            $expert->fill($request->all());
            if ($request->hasFile('avatar')) {
                $helper = new GeneralHelper;
                $helper->deleteFile('uploads/'.$expert->path);
                $file = $helper->uploadFile($request->file('avatar'), 'experts');
                $expert['path'] = $file;
            }
            if ($expert->save()) {
                return back();
            };
            return back();
        } catch (\Exception $e) {

            return redirect('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $expert = Expert::find($id);
            $helper = new GeneralHelper;
            if ($helper->deleteFile('uploads/'.$expert->path)) {
                if ($expert->delete()) {
                    return back();
                }
            };
            return back();
        } catch (\Exception $e) {

            return redirect('/login');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getExperts()
    {
        $experts = Expert::where('category', 'FIRST-TEAM')->get();
        return view('/expert-staff')->with('experts', $experts);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSecondTeamExperts()
    {
        $experts = Expert::where('category', 'OTHER')->get();
        return view('/os-expert-staff')->with('experts', $experts);
    }
}
