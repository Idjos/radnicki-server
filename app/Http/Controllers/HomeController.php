<?php

namespace App\Http\Controllers;

use App\Event;
use App\LatestNew;
use App\Result;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = LatestNew::where('category', 'FIRST-TEAM')->latest()->take(4)->get();
        $events = Event::latest()->latest()->take(5)->get();
        $results = Result::all();
        return view('home')->with([
            'news' => $news,
            'events' => $events,
            'results' => $results
        ]);
    }
}
