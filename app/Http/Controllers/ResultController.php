<?php

namespace App\Http\Controllers;

use App\Result;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = Result::orderBy('type', 'desc')->get();
        return view('admin/results')->with('results', $results);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function show(Result $result)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function edit(Result $result)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Result $result)
    {
//        dd($request->all());
        if ($request->type == 0 && $request->home_go != null && $request->away_go != null) {
//            dd('a');
            $result = Result::where('id', 1)->first();
            $result->fill($request->all());
            if(!isset($request->away_logo)) {
                $result->away_logo = null;
            }
            if(!isset($request->home_logo)) {
                $result->home_logo = null;
            }
            $result->type = 1;
            $result->save();

            $new_result = Result::where('id', 2)->first();
            $new_result->type = 0;
            $new_result->date = Carbon::now();
            $new_result->home_club = 'Домаћи тим';
            $new_result->away_club = 'Гост тим';
            $new_result->away_logo = null;
            $new_result->home_logo = null;
            $new_result->away_go = null;
            $new_result->home_go = null;
            $new_result->save();
        }
        elseif ($request->type == 1) {
//            dd('b');
            $result = Result::where('id', 1)->first();
            $result->fill($request->except(['type']));
            if(!isset($request->away_logo)) {
                $result->away_logo = null;
            }
            if(!isset($request->home_logo)) {
                $result->home_logo = null;
            }
            $result->update();
        }
        elseif ($request->type == 0 && $request->home_go == null && $request->away_go == null) {
//            dd('c');
            $result2 = Result::where('id', 2)->first();
            $result2->fill($request->except(['type']));
            if(!isset($request->away_logo)) {
                $result->away_logo = null;
            }
            if(!isset($request->home_logo)) {
                $result->home_logo = null;
            }
            $result2->update();
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function destroy(Result $result)
    {
        //
    }
}
