<?php

namespace App\Http\Controllers;

use App\Member;
use Illuminate\Http\Request;
use App\Helpers\GeneralHelper;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = Member::all();
        return view('admin/first-team')->with('members', $members);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $member = new Member();
            $member->fill($request->all());
            if ($request->hasFile('avatar')) {
                $helper = new GeneralHelper;
                $file = $helper->uploadFile($request->file('avatar'), 'members');
                $member['path'] = $file;
            }
            if ($member->save()) {
                return back();
            };
            return back();
        } catch (\Exception $e) {
            return redirect('/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $member = Member::find($id);
        $players = Member::all();
        return view('/single-player')->with([
            'member' => $member,
            'players' => $players
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $member = Member::find($id);
            $helper = new GeneralHelper;
            $member->fill($request->all());
            if ($request->hasFile('avatar')) {
                $helper->deleteFile('uploads/'.$member->path);
                $file = $helper->uploadFile($request->file('avatar'), 'members');
                $member['path'] = $file;
            }
            if ($member->save()) {
                return back();
            };
            return back();
        } catch (\Exception $e) {
            return redirect('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $member = Member::find($id);
            $helper = new GeneralHelper;
            if ($helper->deleteFile('uploads/'.$member->path)) {
                if ($member->delete()) {
                    return back();
                }
            };
            return back();
        } catch (\Exception $e) {

            return redirect('/login');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function firstTeam()
    {
        $members = Member::all();
        return view('/first-team')->with('members', $members);
    }
}
