<?php

namespace App\Http\Controllers;

use App\Event;
use App\Helpers\GeneralHelper;
use App\LatestNew;
use Illuminate\Http\Request;

class LatestNewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = LatestNew::where('category', 'FIRST-TEAM')->latest()->paginate(6);
        return view('admin/news')->with('news', $news);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $news = new LatestNew();
            $news->fill($request->all());
            if ($request->hasFile('avatar')) {
                $helper = new GeneralHelper;
                $file = $helper->uploadFile($request->file('avatar'), 'news');
                $news['path'] = $file;
            }
            if ($news->save()) {
                return back();
            };
        return back();
        } catch (\Exception $e) {

            return redirect('/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = LatestNew::with('comments')->find($id);
        return view('/news-single-view')->with('news', $news);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $news = LatestNew::find($id);
            $news->fill($request->all());
            if ($request->hasFile('avatar')) {
                $helper = new GeneralHelper;
                $helper->deleteFile('uploads/'.$news->path);
                $file = $helper->uploadFile($request->file('avatar'), 'news');
                $news['path'] = $file;
            }
            if ($news->save()) {
                return back();
            };
        return back();
        } catch (\Exception $e) {

            return redirect('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $news = LatestNew::find($id);
            $helper = new GeneralHelper;
            if ($helper->deleteFile('uploads/'.$news->path)) {
                if ($news->delete()) {
                    return back();
                }
            };
        } catch (\Exception $e) {
            return redirect('/login');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function firstTeamNews()
    {
        $news = LatestNew::where('category', 'FIRST-TEAM')->latest()->paginate(7);
        $events = Event::latest()->latest()->take(5)->get();
        return view('/news')->with([
            'news' => $news,
            'events' => $events
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function secondTeamsNews()
    {
        $news = LatestNew::where('category', 'OTHER')->latest()->paginate(9);
        return view('/os-news')->with('news', $news);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reports()
    {
        $news = LatestNew::where('category', 'FIRST-TEAM')->where('type', 'ИЗВЕШТАЈ')->latest()->paginate(4);
        $events = Event::latest()->latest()->take(5)->get();
        return view('/reports')->with([
            'news' => $news,
            'events' => $events
        ]);
    }
}
