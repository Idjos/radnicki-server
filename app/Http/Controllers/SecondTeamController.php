<?php

namespace App\Http\Controllers;

use App\Expert;
use App\LatestNew;
use App\SecondTeam;
use Illuminate\Http\Request;
use App\Helpers\GeneralHelper;

class SecondTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = LatestNew::where('category', 'OTHER')->latest()->paginate(6);
        $experts = Expert::where('category', 'OTHER')->get();
        $selections = SecondTeam::all();
        return view('admin/second-teams')->with([
            'news' => $news,
            'experts' => $experts,
            'selections' => $selections,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $item = new SecondTeam();
            $item->fill($request->all());
            if ($request->hasFile('avatar')) {
                $helper = new GeneralHelper;
                $file = $helper->uploadFile($request->file('avatar'), 'second-teams');
                $item['path'] = $file;
            }
            if ($item->save()) {
                return back();
            };
            return back();
        } catch (\Exception $e) {

            return redirect('/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $item = SecondTeam::find($id);
            $item->fill($request->all());
            if ($request->hasFile('avatar')) {
                $helper = new GeneralHelper;
                $helper->deleteFile('uploads/'.$item->path);
                $file = $helper->uploadFile($request->file('avatar'), 'second-teams');
                $item['path'] = $file;
            }
            if ($item->save()) {
                return back();
            };
            return back();
        } catch (\Exception $e) {

            return redirect('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $item = SecondTeam::find($id);
            $helper = new GeneralHelper;
            if ($helper->deleteFile('uploads/'.$item->path)) {
                if ($item->delete()) {
                    return back();
                }
            };
            return back();
        } catch (\Exception $e) {

            return redirect('/login');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function selections()
    {
        $selections = SecondTeam::all();
        return view('/os-selections')->with([
            'selections' => $selections,
        ]);
    }
}
