<?php

namespace App\Http\Controllers;

use App\Helpers\GeneralHelper;
use App\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    /**
        * Store a newly created resource in storage.
        *
        * @param  \Illuminate\Http\Request  $request
        * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        try {
            $image = new Image();
            $image->fill($request->all());
            if ($request->hasFile('image')) {
                $helper = new GeneralHelper;
                $file = $helper->uploadFile($request->file('image'), 'news');
                $image['path'] = $file;
            }
            if ($image->save()) {
                return back();
            };
                return back();
        } catch (\Exception $e) {

            return redirect('/login');
        }
    }


    public function uploadImage(Request $request) {
        try {
            $image = new Image();
            if ($request->hasFile('file')) {
                $helper = new GeneralHelper;
                $file = $helper->uploadFile($request->file('file'), 'news');
                $image['path'] = $file;
            }
            if ($image->save()) {
                return response()->json(['location' => url('/').'/uploads/'.$image['path']], 200);

            };
            return response()->json(['message' => 'Error'], 400);
        } catch (\Exception $e) {

            return response()->json(['message' => $e->getMessage()], 400);
        }
    }
}