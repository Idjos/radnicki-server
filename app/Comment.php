<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'desc', 'email', 'latest_new_id'
    ];

    /**
     * News this comment belongs to.
     */
    public function latestNew() {
        return $this->belongsTo('App\Comment');
    }
}
