<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'home_club', 'away_club', 'home_go', 'away_go', 'date', 'type', 'home_logo', 'away_logo'
    ];
}
