<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;

class GeneralHelper {

    function uploadFile($file, $type) {
        if ($item = Storage::disk('public')->put($type, $file)) {
            return $item;
        }
        return false;
    }

    function deleteFile($path) {
        $link = public_path().'/'.$path;
        if (unlink($link)) {
            return true;
        }
        return false;
    }

}