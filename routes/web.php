<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//PAGES
Route::get('/club-history', function () {return view('club-history');});
Route::get('/club-stars', function () {return view('club-stars');});
Route::get('/sponsors', function () {return view('sponsors');});
Route::get('/stadium', function () {return view('stadium');});
Route::get('/our-fans', function () {return view('our-fans');});
Route::get('/marketing', function () {return view('marketing');});
Route::get('/schedule-results', 'TableController@schedule');
Route::get('/managers', function () {return view('managers');});

//CLUB MANAGERS
Route::get('/toncev', function () {return view('managers/toncev');});
Route::get('/branko-lazic', function () {return view('managers/branko-lazic');});
Route::get('/dejan-propadalo', function () {return view('managers/dejan-propadalo');});
Route::get('/dejan-utvic', function () {return view('managers/dejan-utvic');});
Route::get('/dragan-markovic', function () {return view('managers/dragan-markovic');});
Route::get('/ivan-spasic', function () {return view('managers/ivan-spasic');});
Route::get('/ivana', function () {return view('managers/ivana');});
Route::get('/radan-ilic', function () {return view('managers/radan-ilic');});
Route::get('/velimir-miljkovic', function () {return view('managers/velimir-miljkovic');});
Route::get('/dragan-milenkovic', function () {return view('managers/dragan-milenkovic');});

//CLUB STARS
Route::get('/dragan-holcer', function () {return view('stars/dragan-holcer');});
Route::get('/dragan-pantelic', function () {return view('stars/dragan-pantelic');});
Route::get('/dragan-stojkovic', function () {return view('stars/dragan-stojkovic');});
Route::get('/drda-obradovic', function () {return view('stars/drda-obradovic');});
Route::get('/dusan-mitosevic', function () {return view('stars/dusan-mitosevic');});
Route::get('/miodrag-knezevic', function () {return view('stars/miodrag-knezevic');});
Route::get('/rambo-petkovic', function () {return view('stars/rambo-petkovic');});

//DINAMIC PAGES
Route::get('/', 'HomeController@index');
Route::get('/first-team', 'MemberController@firstTeam');
Route::get('/expert-staff', 'ExpertController@getExperts');
Route::get('/expert-single/{id}', 'ExpertController@show');
Route::get('/news', 'LatestNewController@firstTeamNews');
Route::get('/news-single-view/{id}', 'LatestNewController@show');
Route::get('/reports', 'LatestNewController@reports');
Route::get('/single-player/{id}', 'MemberController@show');
Route::get('/os-expert-staff', 'ExpertController@getSecondTeamExperts');
Route::get('/os-news', 'LatestNewController@secondTeamsNews');
Route::get('/os-selections', 'SecondTeamController@selections');

//COMMENTS CTRL
Route::post('/comment', 'CommentController@store');

Route::middleware(['auth'])->group(function () {
    //ADMIN PAGES
    Route::prefix('admin')->group(function () {

        //SECOND TEAMS
        Route::get('/second-teams', 'SecondTeamController@index');
        Route::post('/second-teams', 'SecondTeamController@store');
        Route::post('/second-teams/{id}', 'SecondTeamController@update');
        Route::get('/delete_second_team/{id}', 'SecondTeamController@destroy');

        //MEMBER CTRL
        Route::get('/first-team', 'MemberController@index');
        Route::post('/member', 'MemberController@store');
        Route::post('/member/{id}', 'MemberController@update');
        Route::get('/delete_member/{id}', 'MemberController@destroy');

        //EXPERTS CTRL
        Route::get('/expert-staff', 'ExpertController@index');
        Route::post('/expert/{id}', 'ExpertController@update');
        Route::post('/expert', 'ExpertController@store');
        Route::get('/delete_expert/{id}', 'ExpertController@destroy');

        //LATEST NEWS CTRL
        Route::get('/news', 'LatestNewController@index');
        Route::post('/news', 'LatestNewController@store');
        Route::post('/news/{id}', 'LatestNewController@update');
        Route::get('/delete_news/{id}', 'LatestNewController@destroy');

        //EVENTS CTRL
        Route::get('/events', 'EventController@index');
        Route::post('/events', 'EventController@store');
        Route::post('/events/{id}', 'EventController@update');
        Route::get('/delete_events/{id}', 'EventController@destroy');

        //COMMENTS CTRL
        Route::get('/delete_comment/{id}', 'CommentController@destroy');

        //RESULTS CTRL
        Route::get('/results', 'ResultController@index');
        Route::post('/results/{id}', 'ResultController@update');

        //TABLE CTRL
        Route::get('/table', 'TableController@index');
        Route::post('/table', 'TableController@store');
    });
});

