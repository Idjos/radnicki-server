$( document ).ready(function() {
    showNav();

    //prikazivanje navigacije pri resizu windowa
    // $(window).resize(function(){
    //     if($( window ).width()<1184) {
    //         $("nav").css("display", "none");
    //         $("nav").css("background", "black");
    //     }else{
    //         $("nav").css("display", "block");
    //         $(".home-header .transp-red,.page-header .transp-red,.page-big-header .transp-red, .news-single-header .transp-red").css("background", "transparent");
    //         $('.red-header .red').css("background", "rgb(229, 57, 53)");
    //     }
    // });

    //prikazivanje navigacije
    function showNav(){
        if($( window ).width()<1184) {
            $("nav").css("display", "none");
            $("nav").css("background", "black");
            $(".menu-bar").click(function () {
                $("nav").slideToggle();
            });
        }else{
            $("nav").css("display", "block");
        }
    }

    //scroll effect na navigaciji
    // $(window).scroll(function(){
    //     if($( window ).width()>1184) {
    //         var scroll = $(window).scrollTop();
    //         if (scroll < 117) {
    //             $(".home-header .transp-red").css("background", "transparent");
    //             $(".page-header .transp-red").css("background", "transparent");
    //             $(".page-big-header .transp-red").css("background", "transparent");
    //             $(".news-single-header .transp-red").css("background", "transparent");
    //             $('.red-header .red').css("background", "rgb(229, 57, 53)");
    //         }
    //         else {
    //             $(".home-header .transp-red").css("background", "rgb(229, 57, 53)");
    //             $(".page-header .transp-red").css("background", "rgb(229, 57, 53)");
    //             $(".page-big-header .transp-red").css("background", "rgb(229, 57, 53)");
    //             $(".news-single-header .transp-red").css("background", "rgb(229, 57, 53)");
    //         }
    //     }
    // });

    //scroll ka naslovu vesti
    $("#trigger-news").click(function() {
        $('html, body').animate({
            scrollTop: ($("#news-main").offset().top)-120
        }, 2000);
    });

    $("#trigger-marketing").click(function() {
        $('html, body').animate({
            scrollTop: ($("#marketing").offset().top)-120
        }, 2000);
    });

    // filter igraca po poziciji
    $( ".first-tim .tim-navigation li a" ).each(function(index) {
        $(this).on("click", function(e){
            e.preventDefault();
            $( ".tim-navigation li a").removeClass('active');
            var position = $(this).attr("id");
            $('#'+position).addClass('active');
            if(position === 'all'){
                $('.player').show();
            }else {
                $('.player[data-set="' + position + '"]').show().addClass('active');
                $( ".player" ).not( '[data-set="'+ position + '"]').hide().removeClass('active');
            }
        });
    });

    //navigacija za prikazivanje liga/cup rezultata
    $( ".scores .tim-navigation li a" ).each(function(index) {
        $(this).on("click", function(e){
            e.preventDefault();
            $( ".tim-navigation li a").removeClass('active');
            var position = $(this).attr("id");
            $('#'+position).addClass('active');

            $('.games[data-set="' + position + '"]').show();
            $( ".games" ).not( '[data-set="'+ position + '"]').hide();
        });
    });

    //prikazivanje razlicitih selekcija u os
    $( ".selections .tim-navigation li a" ).each(function(index) {
        $(this).on("click", function(e){
            e.preventDefault();
            $( ".selections .tim-navigation li a").removeClass('active');
            var position = $(this).attr("id");
            $('#'+position).addClass('active');

            $('.selection[data-set="' + position + '"]').addClass('active');
            $( ".selection" ).not( '[data-set="'+ position + '"]').removeClass('active');
        });
    });
});