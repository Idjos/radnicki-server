@extends('layouts.app')
{{--zvezde kluba single view--}}
@section('content')
    <header class="red-header">
        <a href="{{url('/')}}">
            <div class="logo-small">
                <img src="{{asset('img/header-logo.png')}}">
                <p class="logo-text">ФК РАДНИЧКИ</p>
            </div>
        </a>
        <div class="menu-bar"><i class="fa fa-bars" aria-hidden="true"></i></div>
        <nav class="red">
            <div class="container-big clearfix">
                <ul class="menu-left">
                    <li><a href="{{url('/')}}" class="menu-link ">Почетна</a> </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-link active">О клубу</a>
                        <ul>
                            <li><a href="{{url('/club-history')}}" class="menu-link">Историја клуба</a></li>
                            <li><a href="{{url('/club-stars')}}" class="menu-link">Звезде радничког</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/news')}}" class="menu-link">Вести</a> </li>
                    <li><a href="{{url('/reports')}}" class="menu-link">Извештаји</a> </li>
                    <li>
                        <a href="{{url('/marketing')}}" class="menu-link">Маркетинг</a>
                        <ul>
                            <li><a href="{{url('/sponsors')}}" class="menu-link">Спонзори</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/managers')}}" class="menu-link">Управа клуба</a> </li>
                </ul>
                <a href="{{url('/')}}">
                    <div class="logo">
                        <img src="{{asset('img/header-logo.png')}}">
                        <p class="logo-text">ФК РАДНИЧКИ</p>
                    </div>
                </a>
                <ul class="menu-right">
                    <li><a href="{{url('/schedule-results')}}" class="menu-link">Резултати и распореди</a> </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-link">Тим</a>
                        <ul>
                            <li><a href="{{url('/first-team')}}" class="menu-link">Први тим</a></li>
                            <li><a href="{{url('/expert-staff')}}" class="menu-link">Стручни штаб</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/os-news')}}" class="menu-link">Омладинска школа</a></li>
                    <li><a href="{{url('/stadium')}}" class="menu-link">Стадион</a> </li>
                    <li><a href="{{url('/our-fans')}}" class="menu-link">Наши навијачи</a> </li>
                </ul>
            </div>
        </nav>
    </header>

    <main class="margin-top">
        <section class="stars-radnickog-single">
            <h2 class="fans-text-title">Милован Дрда Обрадовић</h2>
            <img src="{{asset('img/drda.jpg')}}">
            <p class="star-descr-important">
                Рођен је 4. маја 1956. године у Блацу. Био је капитен чувене генерације Радничког која је у сезони 1981/82 играла полуфинале Купа УЕФА.  Фудбал је заиграо у ФК Топличанин из Прокупља, а од 1975. до 1985. године носио је дрес ФК Раднички из Ниша. Каријеру је 1986. године завршио у ФК Војводина. За Раднички је одиграо 553 утакмице, од којих 16 у европским куповима. Капитенску траку на Чаиру носио је од 1981. до 1985. године. Иако је играо на позицији левог бека, био је препознатљив по офанзивној игри. Био је стрелац победоносног поготка у првом полуфиналном мечу Купа УЕФА против Хамбургера на Чаиру (2:1) 1982. године. За А репрезентацију Југославије одиграо је једну утакмицу. За младу репрезентацију Југославије одиграо је 20 утакмица и има велики допринос што је наша селекција до 21 године освојила титулу првака Европе 1978. године.
            </p>
        </section>
        <section class="tickets-apps clearfix">
            <div class="container-big">
                <div class="tickets">
                    <h2 class="horizontal-line-black">резервација карата</h2>
                    <div class="apps-content clearfix">
                        <div class="apps-text">
                            <h2 class="big wrap">резервишите
                                вашу
                                карту!</h2>
                            <p>Пoсетите сајт и резервишите карту за ваш омиљени клуб</p>
                            <a href="http://www.ticketline.rs/" target="_blank" class="tel hover-tel">Ticketline<i class="fas fa-arrow-right font-16 ml-3" style=""></i></a>
                        </div>
                        <img src="{{asset('img/sponsor-big-ticketline.png')}}" class="mob">
                    </div>
                </div>
                <div class="apps">
                    <h2 class="horizontal-line-black">званична брошура клуба</h2>
                    <div class="apps-content clearfix">
                        <div class="apps-text">
                            <h2 class="big wrap">преузмите
                                нашу званичну
                                брошуру</h2>
                            <p>Сазнајте информације о клубу!</p>
                            <a href="{{asset('/pdf/radnicki_katalog.pdf')}}" download class="tel brosura">Брошура<i class="fas fa-arrow-right font-16 ml-3" style=""></i></a>
                        </div>
                        <img src="{{asset('img/brosura.png')}}" class="mob">
                    </div>
                </div>
            </div>
        </section>
    </main>

    {{--MAIN SCRIPT--}}
    <script src="{{asset('js/main.js')}}"></script>
@endsection