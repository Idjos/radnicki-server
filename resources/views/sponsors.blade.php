@extends('layouts.app')
{{--sponzori--}}
@section('content')
    <header class="red-header">
        <a href="{{url('/')}}">
            <div class="logo-small">
                <img src="{{asset('img/header-logo.png')}}">
                <p class="logo-text">ФК РАДНИЧКИ</p>
            </div>
        </a>
        <div class="menu-bar"><i class="fa fa-bars" aria-hidden="true"></i></div>
        <nav class="red">
            <div class="container-big clearfix">
                <ul class="menu-left">
                    <li><a href="{{url('/')}}" class="menu-link">Почетна</a> </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-link">О клубу</a>
                        <ul>
                            <li><a href="{{url('/club-history')}}" class="menu-link">Историја клуба</a></li>
                            <li><a href="{{url('/club-stars')}}" class="menu-link">Звезде радничког</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/news')}}" class="menu-link">Вести</a> </li>
                    <li><a href="{{url('/reports')}}" class="menu-link">Извештаји</a> </li>
                    <li>
                        <a href="{{url('/marketing')}}" class="menu-link active">Маркетинг</a>
                        <ul>
                            <li><a href="{{url('/sponsors')}}" class="menu-link active">Спонзори</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/managers')}}" class="menu-link">Управа клуба</a> </li>
                </ul>
                <a href="{{url('/')}}">
                    <div class="logo">
                        <img src="{{asset('img/header-logo.png')}}">
                        <p class="logo-text">ФК РАДНИЧКИ</p>
                    </div>
                </a>
                <ul class="menu-right">
                    <li><a href="{{url('/schedule-results')}}" class="menu-link">Резултати и распореди</a> </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-link">Тим</a>
                        <ul>
                            <li><a href="{{url('/first-team')}}" class="menu-link">Први тим</a></li>
                            <li><a href="{{url('/expert-staff')}}" class="menu-link">Стручни штаб</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/os-news')}}" class="menu-link">Омладинска школа</a></li>
                    <li><a href="{{url('/stadium')}}" class="menu-link">Стадион</a> </li>
                    <li><a href="{{url('/our-fans')}}" class="menu-link">Наши навијачи</a> </li>
                </ul>
            </div>
        </nav>
    </header>

    <main>
        <section class="sponsors sponsor-main">
            <div class="small-container">
                <h2>ГЛАВНИ СПОНЗОРИ</h2>
                <ul style="margin-bottom: 0px">
                    <li><a href="javascript:void(0);"><img style="width: 250px" src="{{asset('img/sponsor-big-mts.png')}}"><div class="overview"></div></a> </li>
                </ul>
                <ul>
                    <li><a href="javascript:void(0);"><img src="{{asset('img/sponsor-big-sd.png')}}"><div class="overview"></div></a> </li>
                    <li><a href="javascript:void(0);"><img src="{{asset('img/sponsor-big-hummel.png')}}"><div class="overview"></div></a> </li>
{{--                    <li><a href="javascript:void(0);"><img src="{{asset('img/sponsor-big-niska.png')}}"><div class="overview"></div></a> </li>--}}
                    <li><a href="javascript:void(0);"><img src="{{asset('img/sponsor-big-djak.png')}}"><div class="overview"></div></a> </li>
{{--                    <li><a href="javascript:void(0);"><img src="{{asset('img/sponsor-big-ticketline.png')}}"><div class="overview"></div></a> </li>--}}
                    <li><a href="javascript:void(0);"><img src="{{asset('img/sponsor-big-nis2.png')}}"><div class="overview"></div></a> </li>
                    <li><a href="javascript:void(0);"><img src="{{asset('img/sponsor-big-aerodrom.png')}}"><div class="overview"></div></a> </li>
                    <li><a href="javascript:void(0);"><img src="{{asset('img/sponsor-big-tm-inzenjering.png')}}"><div class="overview"></div></a> </li>
                    <li><a href="javascript:void(0);"><img src="{{asset('img/sponsor-big-dunav.png')}}"><div class="overview"></div></a> </li>

                </ul>
                <h2>МЕДИЈСКИ ПАРТНЕРИ</h2>
                <ul>
                    <li><a href="javascript:void(0);"><img src="{{asset('img/sponsor-big-sd.png')}}"><div class="overview"></div></a> </li>
                    <li><a href="javascript:void(0);"><img src="{{asset('img/zona.png')}}"><div class="overview"></div></a> </li>
                    <li><a href="javascript:void(0);"><img src="{{asset('img/hotsport.png')}}"><div class="overview"></div></a> </li>
                    <li><a href="javascript:void(0);"><img src="{{asset('img/sponsor-big-RTVBA.png')}}"><div class="overview"></div></a> </li>
{{--                    <li><a href="javascript:void(0);"><img src="{{asset('img/sponsor-big-juznasrbija.png')}}"><div class="overview"></div></a> </li>--}}
{{--                    <li><a href="javascript:void(0);"><img src="{{asset('img/sponsor-big-Juzne_vesti.png')}}"><div class="overview"></div></a> </li>--}}
{{--                    <li><a href="javascript:void(0);"><img src="{{asset('img/sponsor-big-niska_televizija.png')}}"><div class="overview"></div></a> </li>--}}
                    <li><a href="javascript:void(0);"><img src="{{asset('img/sponsor-big-narodne-novine.png')}}"><div class="overview"></div></a> </li>
                </ul>
            </div>
        </section>
    </main>

    {{--MAIN SCRIPT--}}
    <script src="{{asset('js/main.js')}}"></script>
@endsection
