@extends('layouts.app')
{{--raspored-rezultati--}}
@section('content')
    <style>
        .position .team img {
            display: none;
        }
    </style>
    <header class="page-header scores-header">
        <a href="{{url('/')}}">
            <div class="logo-small">
                <img src="{{asset('img/header-logo.png')}}">
                <p class="logo-text">ФК РАДНИЧКИ</p>
            </div>
        </a>
        <div class="menu-bar"><i class="fa fa-bars" aria-hidden="true"></i></div>
        <nav class="transp-red header-red">
            <div class="container-big clearfix">
                <ul class="menu-left">
                    <li><a href="{{url('/')}}" class="menu-link">Почетна</a> </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-link">О клубу</a>
                        <ul>
                            <li><a href="{{url('/club-history')}}" class="menu-link">Историја клуба</a></li>
                            <li><a href="{{url('/club-stars')}}" class="menu-link">Звезде радничког</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/news')}}" class="menu-link">Вести</a> </li>
                    <li><a href="{{url('/reports')}}" class="menu-link">Извештаји</a> </li>
                    <li>
                        <a href="{{url('/marketing')}}" class="menu-link">Маркетинг</a>
                        <ul>
                            <li><a href="{{url('/sponsors')}}" class="menu-link">Спонзори</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/managers')}}" class="menu-link">Управа клуба</a> </li>
                </ul>
                <a href="{{url('/')}}">
                    <div class="logo">
                        <img src="{{asset('img/header-logo.png')}}">
                        <p class="logo-text">ФК РАДНИЧКИ</p>
                    </div>
                </a>
                <ul class="menu-right">
                    <li><a href="{{url('/schedule-results')}}" class="menu-link active">Резултати и распореди</a> </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-link">Тим</a>
                        <ul>
                            <li><a href="{{url('/first-team')}}" class="menu-link">Први тим</a></li>
                            <li><a href="{{url('/expert-staff')}}" class="menu-link">Стручни штаб</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/os-news')}}" class="menu-link">Омладинска школа</a></li>
                    <li><a href="{{url('/stadium')}}" class="menu-link">Стадион</a> </li>
                    <li><a href="{{url('/our-fans')}}" class="menu-link">Наши навијачи</a> </li>
                </ul>
            </div>
        </nav>
    </header>

    <main class="page-margin-top-negative">
        <div class="container-big scores">
            <h1 class="page-title">РЕЗУЛТАТИ И
                <span>РАСПОРЕДИ</span></h1>
            <section class="games games-table clearfix" data-set="prvenstvo" >
                <div class="game-schedule" style="height: 800px">
                    <!--utakmice prvenstva-->
                    <iframe style="width: 100%;pointer-events: none;" src="http://bezmotike.com/webmasters/clubs/index.php?club=Radnicki_Nis&font=Arial&fontsize=11px&fontcolor=454545&matchfont=Arial&matchcolor=000000&acolor=454545&aover=991B1F&losebkg=DF7F90&losecolor=000000&drawbkg=DEE7B1&drawcolor=000000&winbkg=6FB790&wincolor=000000&bkg=FFFFFF&yearbkg=991B1F&yearcolor=FFFFFF" style="pointer-events: none;" border=0 frameborder=0 width=560 height=800px scrolling=no></iframe>
                </div>
                <div class="table-responsive" style="width: 48%">
                    <!--tabela prvenstva-->
                    <table style="background: white; font-size: 15px" class="table">
                        <thead>
                        <tr class="text-center">
                            <th scope="col">Позиција</th>
                            <th scope="col">Име тима</th>
                            <th scope="col">Број одиграних мечева</th>
                            <th scope="col">Број бодова</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($teams as $key => $team)
                            <tr @if($team->name == 'Раднички Ниш') style="background: rgb(229, 57, 53); color: white" @endif class="text-center">
                                <th scope="row">{{$key + 1}}</th>
                                <td>
                                    {{$team->name}}
                                </td>
                                <td>
                                    {{$team->played_games}}
                                </td>
                                <td>
                                    {{$team->points}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{--<iframe frameborder="0" style="pointer-events: none;" scrolling="no" width="520" height="700" src="https://www.fctables.com/serbia/super-liga/iframe/?type=table&lang_id=2&country=189&template=106&team=&timezone=Europe/Belgrade&time=24&po=1&ma=1&wi=1&dr=1&los=1&gf=1&ga=1&gd=1&pts=1&ng=1&form=1&width=520&height=700&font=Verdana&fs=12&lh=22&bg=FFFFFF&fc=333333&logo=1&tlink=1&ths=1&thb=1&thba=FFFFFF&thc=000000&bc=dddddd&hob=f5f5f5&hobc=ebe7e7&lc=333333&sh=1&hfb=1&hbc=3bafda&hfc=FFFFFF"></iframe><div style="text-align:center;"></div><a href="https://www.fctables.com/serbia/super-liga/" rel="nofollow">FcTables.com</a>--}}
                </div>
            </section>
        </div>
        <section class="tickets-apps clearfix">
            <div class="container-big">
                <div class="tickets">
                    <h2 class="horizontal-line-black">резервација карата</h2>
                    <div class="apps-content clearfix">
                        <div class="apps-text">
                            <h2 class="big wrap">резервишите
                                вашу
                                карту!</h2>
                            <p>Пoсетите сајт и резервишите карту за ваш омиљени клуб</p>
                            <a href="http://www.ticketline.rs/" target="_blank" class="tel hover-tel">Ticketline<i class="fas fa-arrow-right font-16 ml-3" style=""></i></a>
                        </div>
                        <img src="{{asset('img/sponsor-big-ticketline.png')}}" class="mob">
                    </div>
                </div>
                <div class="apps">
                    <h2 class="horizontal-line-black">званична брошура клуба</h2>
                    <div class="apps-content clearfix">
                        <div class="apps-text">
                            <h2 class="big wrap">преузмите
                                нашу званичну
                                брошуру</h2>
                            <p>Сазнајте информације о клубу!</p>
                            <a href="{{asset('/pdf/radnicki_katalog.pdf')}}" download class="tel brosura">Брошура<i class="fas fa-arrow-right font-16 ml-3" style=""></i></a>
                        </div>
                        <img src="{{asset('img/brosura.png')}}" class="mob">
                    </div>
                </div>
            </div>
        </section>
    </main>

    {{--MAIN SCRIPT--}}
    <script src="{{asset('js/main.js')}}"></script>
@endsection
