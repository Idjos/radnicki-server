@extends('layouts.app')
{{--POCETNA--}}
@section('content')
    <header class="home-header" id="image">
        <a href="{{url('/')}}">
            <div class="logo-small">
                <img src="{{asset('img/header-logo.png')}}">
                <p class="logo-text">ФК РАДНИЧКИ</p>
            </div>
        </a>
        <div class="menu-bar"><i class="fa fa-bars" aria-hidden="true"></i></div>
        <nav class="transp-red header-red">
            <div class="container-big clearfix">
                <ul class="menu-left">
                    <li><a href="{{url('/')}}" class="menu-link active">Почетна</a> </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-link">О клубу</a>
                        <ul>
                            <li><a href="{{url('/club-history')}}" class="menu-link">Историја клуба</a></li>
                            <li><a href="{{url('/club-stars')}}" class="menu-link">Звезде радничког</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/news')}}" class="menu-link">Вести</a> </li>
                    <li><a href="{{url('/reports')}}" class="menu-link">Извештаји</a> </li>
                    <li>
                        <a href="{{url('/marketing')}}" class="menu-link">Маркетинг</a>
                        <ul>
                            <li><a href="{{url('/sponsors')}}" class="menu-link">Спонзори</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/managers')}}" class="menu-link">Управа клуба</a> </li>
                </ul>
                <a href="{{url('/')}}">
                    <div class="logo">
                        <img src="{{asset('img/header-logo.png')}}">
                        <p class="logo-text">ФК РАДНИЧКИ</p>
                    </div>
                </a>
                <ul class="menu-right">
                    <li><a href="{{url('/schedule-results')}}" class="menu-link">Резултати и распореди</a> </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-link">Тим</a>
                        <ul>
                            <li><a href="{{url('/first-team')}}" class="menu-link">Први тим</a></li>
                            <li><a href="{{url('/expert-staff')}}" class="menu-link">Стручни штаб</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/os-news')}}" class="menu-link">Омладинска школа</a></li>
                    <li><a href="{{url('/stadium')}}" class="menu-link">Стадион</a> </li>
                    <li><a href="{{url('/our-fans')}}" class="menu-link">Наши навијачи</a> </li>
                </ul>
            </div>
        </nav>
        <div class="social-menu container-big">
            <ul>
                <li><a href="{{url('https://www.facebook.com/RadnickiNisOfficial/?ref=br_rs')}}" target="_blank"><span class="social-name">Facebook</span><i class="fab fa-facebook"></i></a></li>
                <li><a href="{{url('https://www.instagram.com/fkradnickinis/?hl=sr')}}" target="_blank"><span class="social-name">Instagram</span><i class="fab fa-instagram"></i></a></li>
                <li><a href="{{url('https://plus.google.com/105547925413940230771')}}" target="_blank"><span class="social-name">Google +</span><i class="fab fa-google-plus-g"></i></a></li>
                <li><a href="{{url('https://twitter.com/fkradnickinisrs')}}" target="_blank"><span class="social-name">Twitter</span><i class="fab fa-twitter"></i></a></li>
                <li><a href="{{url('https://www.youtube.com/channel/UCZsRDmK2EHfsXQ7f6Urrf0A')}}" target="_blank"><span class="social-name">You Tube</span><i class="fab fa-youtube"></i></a></li>
            </ul>
        </div>
        <div class="container-big">
            <div class="header-news">
                <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
                    <div class="carousel-inner">
                        @foreach($news as $key => $new)
                        <div class="carousel-item @if($key == 0) active @endif">
                            <p class="category-news category-news-red margin-bottom-small home-slider-text">{{$new->type}}</p>
                            <h1 class="header-headings margin-bottom-small">{{$new->name}}</h1>
                            <div class="white margin-bottom-small home-slider-text">{!! strip_tags(str_limit($new->desc,200),  '<p><br>') !!}</div>
                            <a href="{{asset('/news-single-view/'.$new->id)}}" class="button button-red button-headings">Прочитајте више</a>
                        </div>
                        @endforeach
                    </div>
                    <ol class="carousel-indicators">
                        @foreach($news as $key => $new)
                            <li data-target="#carouselExampleFade" data-slide-to="{{$key}}" @if($key == 0)class="active"@endif></li>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
    </header>
    <script>
        $(function(){
            var array=[];
            var a;
            @foreach($news as $new)
                array.push('<?php echo 'uploads/'.$new->path; ?>');
            @endforeach
            var carouselEl = $('.carousel');
            var carouselItems = carouselEl.find('.carousel-item');
            $('#image').css('background-image', "url("+array[carouselItems.siblings('.active').index()]+")");
            carouselEl.carousel({
            }).on('slid.bs.carousel', function (event) {
                $('#image').css('background-image', "url("+array[carouselItems.siblings('.active').index()]+")");
            })
        })
    </script>
    <main class="margin-top-negative">
        <div class="container-big">
            <h2 class="horizontal-line horizontal-line-white">Вести</h2>
        </div>
        <section class="news-events">
            <div class="container-big">
                <div class="all-news">
                    <!--vesti-->

                    <!--u newsrow po dve vesti-->
                    @foreach($news as $key => $new)
                        @if($key != 2)
                            <div class="news-row">
                                <a href="{{asset('news-single-view/'.$new->id)}}">
                                    <div class="news">
                                        <div style="background-image: url('{{asset('uploads/'.$new->path)}}');" class="img-homepage"></div>
                                        <!-- <img src="{{asset('uploads/'.$new->path)}}" class="news-img"> <!--featured img za vest--> -->
                                        <div class="news-content p-4">
                                            <p class="category-news category-news-red">{{$new->type}}</p><!--kategorija-->
                                            <a href="{{asset('news-single-view/'.$new->id)}}" class="news-link"><h2>{{$new->name}}</h2></a><!--naslov ujedno i link ka single view-->
                                            <p>{!! strip_tags(str_limit($new->desc,100),  '<p><br>') !!}</p> <!--excerpt vesti -->
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @else
                            <a href="{{asset('news-single-view/'.$new->id)}}">
                                <div class="news news-important mr-4">
                                    {{--<img src="{{asset('uploads/'.$new->path)}}" class="news-img">--}}
                                    <div style="background-image: url('{{asset('uploads/'.$new->path)}}');" class="img-homepage"></div>
                                    <div class="news-content news-content-important">
                                        <p class="category-news category-news-red">{{$new->type}}</p>
                                        <a href="{{asset('news-single-view/'.$new->id)}}" class="news-link"><h2>{{str_limit($new->name,70)}}</h2></a>
{{--                                        <p>{!! strip_tags(str_limit($new->desc,100),  '<p><br>') !!}</p>--}}
                                    </div>
                                </div>
                            </a>
                        @endif
                    @endforeach
                    <div class="news news-red fixed-col">
                        <div class="news-content-red">
                            <h2>ПРОЧИТАЈТЕ СВЕ ВЕСТИ</h2>
                            <p class="white"></p>
                            <a href="{{asset('/news')}}" class="button button-white">ПОГЛЕДАЈТЕ ВЕСТИ</a>
                        </div>
                        <img src="{{asset('img/news-logo.png')}}" class="img-red">
                    </div>
                </div>
                <div class="events" style="padding: 20px; background-color: #fff; @if(count($events) == 0) display: none; @endif">
                    <div class="row news-blue">
                        @foreach($events as $event)
                        <div class="col-12">
                            <div class="blue-news">
                                <h3>{{date('d', strtotime($event->date))}}</h3>
                                <h5>{{date('M', strtotime($event->date))}}</h5>
                            </div>
                            <div class="text-news">
                                <h4>{{$event->name}}</h4>
                                <h6>{{$event->desc}}</h6>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
        <section class="container-big tables-schedules clearfix">

            {{--<div class="table-score">--}}
                {{--<h2 class="horizontal-line-black">последњи меч</h2>--}}
                {{--<div class="table-content" style="height: 256px;">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-12 text-center team-results pt-5" id="match-time"></div>--}}
                    {{--</div>--}}
                    {{--<div class="row" id="results">--}}
                        {{--<div class="col-4 col-md-5 mt-5 team-results text-center align-self-center">--}}
                            {{--<div id="team1"></div>--}}
                            {{--<div id="team1-name"></div>--}}
                        {{--</div>--}}
                        {{--<div class="col-4 col-md-2 mt-5 team-results-type text-center font-weight-bold">--}}
                            {{--<span id="score"></span><br>--}}
                            {{--(<span id="halftime"></span>)--}}
                        {{--</div>--}}
                        {{--<div class="col-4 col-md-5 mt-5 team-results text-center align-self-center">--}}
                            {{--<div id="team2"></div>--}}
                            {{--<div id="team2-name"></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div id="no-results" class="mt-5 team-results text-center align-self-center">--}}
                        {{--<h1>Чекање резултата</h1>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="schedules clearfix">--}}
                {{--<h2 class="horizontal-line-black">Календар</h2>--}}
                {{--<div class="last-game">--}}

                {{--</div>--}}
                {{--<div class="next-game">--}}

                {{--</div>--}}
            {{--</div>--}}
        </section>
        <section class="container-big tables-schedules clearfix">
            <div class="row">
                @foreach($results as $result)
                <div class="col-12 col-md-6">
                    <div class="table-score">
                        <h2 class="horizontal-line-black">@if($result->type == 1) последњи меч @else будући меч @endif</h2>
                        <div class="table-content" style="height: 220px;">
                            <div class="row">
                                <div class="col-12 text-center team-results pt-5" id="match-time">{{date('d.m.Y.', strtotime($result->date))}}</div>
                            </div>
                            <div class="row mt-4" id="results">
                                <div class="col-4 col-md-5 mt-5 team-results text-center align-self-center">
                                    <div id="team1-name">
                                        @if($result->home_logo)<img src="{{asset($result->home_logo)}}"> @else <img src="{{asset('img/new-images/clubs/default-logo.png')}}"> @endif
                                        &nbsp;<span class="d-none d-xl-inline">{{$result->home_club}}</span>
                                    </div>
                                </div>
                                <div class="col-4 col-md-2 mt-5 pt-3 team-results-type text-center font-weight-bold">
                                    <span id="score">{{$result->home_go}} - {{$result->away_go}}</span>
                                </div>
                                <div class="col-4 col-md-5 mt-5 team-results text-center align-self-center">
                                    <div id="team2-name">
                                        <span class="d-none d-xl-inline">{{$result->away_club}}</span> &nbsp;
                                        @if($result->away_logo)<img src="{{asset($result->away_logo)}}"> @else <img src="{{asset('img/new-images/clubs/default-logo.png')}}"> @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </section>
        <div class="slider">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="{{asset('img/new-images/home/slika9.jpg')}}" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('img/new-images/home/slika10.jpg')}}" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('img/new-images/home/slika1.jpg')}}" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('img/new-images/home/slika2.jpg')}}" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('img/new-images/home/slika3.jpg')}}" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('img/new-images/home/slika8.jpg')}}" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('img/new-images/home/slika4.jpg')}}" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('img/new-images/home/slika5.jpg')}}" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('img/new-images/home/slika6.jpg')}}" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('img/new-images/home/slika7.jpg')}}" alt="First slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>

        <section class="tickets-apps clearfix">
            <div class="container-big">
                <div class="tickets">
                    <h2 class="horizontal-line-black">резервација карата</h2>
                    <div class="apps-content clearfix">
                        <div class="apps-text">
                            <h2 class="big wrap">резервишите
                            вашу
                            карту!</h2>
                            <p>Пoсетите сајт и резервишите карту за ваш омиљени клуб</p>
                            <a href="http://www.ticketline.rs/" target="_blank" class="tel hover-tel">Ticketline<i class="fas fa-arrow-right font-16 ml-3" style=""></i></a>
                        </div>
                        <img src="{{asset('img/sponsor-big-ticketline.png')}}" class="mob">
                    </div>
                </div>
                <div class="apps">
                    <h2 class="horizontal-line-black">званична брошура клуба</h2>
                    <div class="apps-content clearfix">
                        <div class="apps-text">
                            <h2 class="big wrap">преузмите
                                нашу званичну
                                брошуру</h2>
                            <p>Сазнајте информације о клубу!</p>
                            <a href="{{asset('/pdf/radnicki_katalog.pdf')}}" download class="tel brosura">Брошура<i class="fas fa-arrow-right font-16 ml-3" style=""></i></a>
                        </div>
                        <img src="{{asset('img/brosura.png')}}" class="mob">
                    </div>
                </div>
            </div>
        </section>

        <div class="map" id="map">
        </div>
    </main>

    <a href="https://www.livescore.bz" style="display: none">Radnicki Nis scores</a>

    {{--<script type="text/javascript" src="https://www.livescore.bz/api.1.1.js" api="livescore" callback="poziv" BZwidth="300" BZcolor="#000000" async template="simple-auto"></script>--}}

    <script>
        // function poziv(event) {
        //     if (event[0]) {
        //         if (event[0].awaylogo == "") {
        //             event[0].awaylogo = '<img src="img/default-badge.png">'
        //         }
        //         if (event[0].homelogo == "") {
        //             event[0].homelogo = '<img src="img/default-badge.png">'
        //         }
        //         document.getElementById('team1').innerHTML = event[0].homelogo;
        //         document.getElementById('team1-name').innerHTML = event[0].hometeam;
        //         document.getElementById('team2').innerHTML = event[0].awaylogo;
        //         document.getElementById('team2-name').innerHTML = event[0].awayteam;
        //         document.getElementById('halftime').innerHTML = event[0].halftime;
        //         document.getElementById('score').innerHTML = event[0].score;
        //         document.getElementById('match-time').innerHTML = event[0].datefull;
        //         $('#no-results').hide();
        //         $('#results').show();
        //     }
        //     else {
        //         $('#no-results').show();
        //         $('#results').hide();
        //     }
        // }
    </script>
    {{--MAIN SCRIPT--}}
    <script src="{{asset('js/main.js')}}"></script>

    {{--MAP--}}
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"
          integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ=="
          crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"
            integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw=="
            crossorigin=""></script>
    <script>
        var mymap = L.map('map').setView([43.315025, 21.908751], 16);
        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1Ijoid3d3b2xmeSIsImEiOiJjamprYTFuY3IxdXl0M3dteGk3NWtpcDJ0In0.YuP8_vETG2mbAvQvlnZVnQ'
        }).addTo(mymap);
        var marker = L.marker([43.315025, 21.908751]).addTo(mymap);
    </script>
    {{--END MAP--}}
@endsection
