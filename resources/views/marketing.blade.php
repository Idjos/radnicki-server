@extends('layouts.app')
{{--marketing--}}
@section('content')
    <header class="news-single-header marketing-header">
        <div class="immag" style="height: 700px">
            <img src="{{asset('img/new-images/others/marketing.jpg')}}">
        </div>
        <a href="{{url('/')}}">
            <div class="logo-small">
                <img src="{{asset('img/header-logo.png')}}">
                <p class="logo-text">ФК РАДНИЧКИ</p>
            </div>
        </a>
        <div class="menu-bar"><i class="fa fa-bars" aria-hidden="true"></i></div>
        <nav class="transp-red header-red">
            <div class="container-big clearfix">
                <ul class="menu-left">
                    <li><a href="{{url('/')}}" class="menu-link">Почетна</a> </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-link">О клубу</a>
                        <ul>
                            <li><a href="{{url('/club-history')}}" class="menu-link">Историја клуба</a></li>
                            <li><a href="{{url('/club-stars')}}" class="menu-link">Звезде радничког</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/news')}}" class="menu-link">Вести</a> </li>
                    <li><a href="{{url('/reports')}}" class="menu-link">Извештаји</a> </li>
                    <li>
                        <a href="{{url('/marketing')}}" class="menu-link active">Маркетинг</a>
                        <ul>
                            <li><a href="{{url('/sponsors')}}" class="menu-link">Спонзори</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/managers')}}" class="menu-link">Управа клуба</a> </li>
                </ul>
                <a href="{{url('/')}}">
                    <div class="logo">
                        <img src="{{asset('img/header-logo.png')}}">
                        <p class="logo-text">ФК РАДНИЧКИ</p>
                    </div>
                </a>
                <ul class="menu-right">
                    <li><a href="{{url('/schedule-results')}}" class="menu-link">Резултати и распореди</a> </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-link">Тим</a>
                        <ul>
                            <li><a href="{{url('/first-team')}}" class="menu-link">Први тим</a></li>
                            <li><a href="{{url('/expert-staff')}}" class="menu-link">Стручни штаб</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/os-news')}}" class="menu-link">Омладинска школа</a></li>
                    <li><a href="{{url('/stadium')}}" class="menu-link">Стадион</a> </li>
                    <li><a href="{{url('/our-fans')}}" class="menu-link">Наши навијачи</a> </li>
                </ul>
            </div>
        </nav>
        <a href="javascript:void(0);" class="go-down" id="trigger-marketing"><img src="{{asset('img/go-down.png')}}"></a>
    </header>

    <main>
        <section class="marketing" id="marketing">
            <h1 class="marketing-heading">МАРКЕТИНГ</h1>
            <div class="row mt-5">
                <div class="col-12 col-md-6">
                    <p>Важно нам је да будемо лидери у спорту! Важно нам је да код омладине развијемо такмичарски дух и жељу да буду најбољи! Важна нам је промоција здравог начина живота! Важно нам је да нас препознају!
                    </p>
                    <p>Један од круцијалних планова, поред постизања најбољих резултата и циља да будемо међу најбољим фудбалским клубовима у Србији, али и Европи, је пре свега препознатљивост бренда ФК Раднички.
                    </p>
                    <p>Бренд постоји од давне 1923. Са растом и развојем клуба, развијао се и однос према маркетингу, уобличавао и пратио савремене трендове али притом чувајући дух и обличје иницијалног обележја.
                    </p>
                    <p>Низом акција кроз промоцију бренда клуба желимо да укажемо на важност бављења спортом, пре свега у локалној заједници. То се огледа кроз мноштво  дружења са основцима, средњошколцима, децом са посебним потребама, али и кроз учешћа у низу хуманитарних и друштвено одговорних акција којима смо се придружили.
                    </p>
                    <p>Свакодневно истражујемо потребе кроз креирање циљева, а у сврху још боље промоције нашег клуба, како кроз директне сусрете са нашим навијачима, тако и кроз дигитални маркетинг.
                    </p>
                    <p>Мисија нам је да ускладимо амбиције и циљеве спортског и маркетинг сектора ФК Раднички и постанемо још препознатљивији  кроз спортске резултате и кроз бренд клуба.
                    </p>
                    <p>ДРЕС ФК РАДНИЧКИ МОЖЕ СЕ КУПИТИ У ПРОДАВНИЦАМА "ЂАК СПОРТА" !</p>
                </div>
                <div class="col-12 col-md-6">
                    <img style="width: 100%" src="{{asset('img/new-images/others/player.jpg')}}">
                </div>
            </div>
            <br><br>
            <p>Информације о акредитацији новинара и формулар за акредитовање можете преузети <a style="color: red; text-decoration: underline" href="{{asset('/pdf/formular_za_press_akreditaciju.pdf')}}" download>овде</a>. Пријаве можете слати на e-mail: <a style="color: red" href="mailto:prsluzba@fkradnickinis.rs">prsluzba@fkradnickinis.rs</a>.</p>
            <div class="contact-info text-center">
                <h3 class="contact-title">КОНТАКТ ИНФОРМАЦИЈЕ</h3>
                <p class="contact-content"><span>Адреса:</span>Градски стадион Чаир<br/>
                    9. бригаде ББ 18000 Ниш</p>
                <p class="contact-content"><span>Телефон / факс:</span>+381 18 527 259</p>
                <p class="contact-content"><span>Е-пошта:</span>office@fkradnickinis.rs</p>
            </div>
            {{--<div class="contact-content">--}}
                {{--<h3 class="contact-title">Контакт особа</h3>--}}
                {{--<p class="contact-content"><span>Телефон:</span> +381604440891--}}
                    {{--<br/>Милена Пајкић</p>--}}
            {{--</div>--}}
        </section>
        <section class="tickets-apps clearfix">
            <div class="container-big">
                <div class="tickets">
                    <h2 class="horizontal-line-black">резервација карата</h2>
                    <div class="apps-content clearfix">
                        <div class="apps-text">
                            <h2 class="big wrap">резервишите
                                вашу
                                карту!</h2>
                            <p>Пoсетите сајт и резервишите карту за ваш омиљени клуб</p>
                            <a href="http://www.ticketline.rs/" target="_blank" class="tel hover-tel">Ticketline<i class="fas fa-arrow-right font-16 ml-3" style=""></i></a>
                        </div>
                        <img src="{{asset('img/sponsor-big-ticketline.png')}}" class="mob">
                    </div>
                </div>
                <div class="apps">
                    <h2 class="horizontal-line-black">званична брошура клуба</h2>
                    <div class="apps-content clearfix">
                        <div class="apps-text">
                            <h2 class="big wrap">преузмите
                                нашу званичну
                                брошуру</h2>
                            <p>Сазнајте информације о клубу!</p>
                            <a href="{{asset('/pdf/radnicki_katalog.pdf')}}" download class="tel brosura">Брошура<i class="fas fa-arrow-right font-16 ml-3" style=""></i></a>
                        </div>
                        <img src="{{asset('img/brosura.png')}}" class="mob">
                    </div>
                </div>
            </div>
        </section>
    </main>

    {{--MAIN SCRIPT--}}
    <script src="{{asset('js/main.js')}}"></script>
@endsection