<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Раднички Ниш</title>
    <link rel="shortcut icon" type="image/png" href="{{asset('img/header-logo.png')}}"/>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

@if(Request::is('admin/*') || Request::is('login') )
    <link rel="stylesheet" type="text/css" href="{{  asset ('css/admin.css')}}">
    @else
    <link rel="stylesheet" type="text/css" href="{{  asset ('css/style.css')}}">
    @endif
    {{--Scripts--}}
    <script src="{{ asset('js/app.js') }}"></script>

    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=2p8bwwz8bstlt0c73ollfemhrtos9q8scd3w2n1fd1kjeh37"></script>
    <script type="text/javascript">
        var tiny1 = tinymce.init({
            selector: '#desc-tiny',
            plugins: 'image code',
            image_title: true,
            automatic_uploads: true,
            images_upload_url: '../api/image',
            file_picker_types: 'image',
            file_picker_callback: function(cb, value, meta) {
                var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');
                input.onchange = function () {
                    var file = this.files[0];

                    var reader = new FileReader();
                    reader.onload = function () {
                        var id = 'blobid' + (new Date()).getTime();
                        var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                        var base64 = reader.result.split(',')[1];
                        var blobInfo = blobCache.create(id, file, base64);
                        blobCache.add(blobInfo);

                        // call the callback and populate the Title field with the file name
                        cb(blobInfo.blobUri(), {title: file.name});
                    };
                    reader.readAsDataURL(file);
                };

                input.click();
            }
        });

        var tiny2 = tinymce.init({
            selector: '.matches-tiny',
            plugins: 'image code',
            image_title: true,
            automatic_uploads: true,
            images_upload_url: '../api/image',
            file_picker_types: 'image',
            file_picker_callback: function(cb, value, meta) {
                var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');
                input.onchange = function () {
                    var file = this.files[0];

                    var reader = new FileReader();
                    reader.onload = function () {
                        var id = 'blobid' + (new Date()).getTime();
                        var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                        var base64 = reader.result.split(',')[1];
                        var blobInfo = blobCache.create(id, file, base64);
                        blobCache.add(blobInfo);

                        // call the callback and populate the Title field with the file name
                        cb(blobInfo.blobUri(), {title: file.name});
                    };
                    reader.readAsDataURL(file);
                };

                input.click();
            }
        });

        var tiny3 = tinymce.init({
            selector: '.edit-desc-tiny',
            plugins: 'image code',
            image_title: true,
            automatic_uploads: true,
            images_upload_url: '../api/image',
            file_picker_types: 'image',
            file_picker_callback: function(cb, value, meta) {
                var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');
                input.onchange = function () {
                    var file = this.files[0];

                    var reader = new FileReader();
                    reader.onload = function () {
                        var id = 'blobid' + (new Date()).getTime();
                        var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                        var base64 = reader.result.split(',')[1];
                        var blobInfo = blobCache.create(id, file, base64);
                        blobCache.add(blobInfo);

                        // call the callback and populate the Title field with the file name
                        cb(blobInfo.blobUri(), {title: file.name});
                    };
                    reader.readAsDataURL(file);
                };

                input.click();
            }
        });
    </script>
</head>
<body
    @if(!(Request::is('club-history') || Request::is('marketing') || Request::is('sponsors') || Request::is('news-single-view')))
        class="background"
    @endif
>
    <div id="app">
        @auth
        @if(Request::is('admin/*') || Request::is('login') )
        <nav class="navbar navbar-expand-lg navbar-light bg-light custom-navbar">
            <div class="container-fluid">
                <a class="navbar-brand" href="javascript:void(0);"><img style="width: 20px;" src="{{ url('img/logo.png') }}"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                        <li class="nav-item {{ Request::is('admin/news') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ url('admin/news') }}">Новости <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item  {{ Request::is('admin/events') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ url('admin/events') }}">Календар</a>
                        </li>
                        <li class="nav-item {{ Request::is('admin/first-team') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ url('admin/first-team') }}">Први тим</a>
                        </li>
                        <li class="nav-item {{ Request::is('admin/expert-staff') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ url('admin/expert-staff') }}">Стручни штаб</a>
                        </li>
                        <li class="nav-item  {{ Request::is('admin/second-teams') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ url('admin/second-teams') }}">Категорије</a>
                        </li>
                        <li class="nav-item  {{ Request::is('admin/results') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ url('admin/results') }}">Резултати</a>
                        </li>
                        <li class="nav-item  {{ Request::is('admin/table') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ url('admin/table') }}">Табела</a>
                        </li>
                    </ul>
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item mr-3">
                            <a class="a-tag" href="{{ url('/') }}">
                                Почетна
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="a-tag" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                Одјави се
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        @endif
        @endauth
        <div @if(Request::is('admin/*') || Request::is('login') ) class="mt-4 mt-md-5 pb-5" @endif>
            @yield('content')

            @if(!(Request::is('admin/*') || Request::is('login')) )
            <main>
                @if(!(Request::is('sponsors')))
                <section class="sponsors">
                    <h2 class="">ГЛАВНИ СПОНЗОРИ</h2>
                    <ul>
                        <li><a target="_blank" href="javascript:void(0);"><img src="{{asset('img/sponsor-sd.png')}}"><div class="overview"></div></a> </li>
                        <li><a target="_blank" href="javascript:void(0);"><img src="{{asset('img/sponsor-hummel.png')}}"><div class="overview"></div></a> </li>
{{--                        <li><a target="_blank" href="javascript:void(0);"><img src="{{asset('img/sponsor-nis.png')}}"><div class="overview"></div></a> </li>--}}
                        <li><a target="_blank" href="javascript:void(0);"><img src="{{asset('img/sponsor-djak.png')}}"><div class="overview"></div></a> </li>
{{--                        <li><a target="_blank" href="javascript:void(0);"><img src="{{asset('img/sponsor-ticketline.png')}}"><div class="overview"></div></a> </li>--}}
                        <li><a target="_blank" href="javascript:void(0);"><img src="{{asset('img/sponsor-mts.png')}}"><div class="overview"></div></a> </li>
                        <li><a target="_blank" href="javascript:void(0);"><img src="{{asset('img/sponsor-nis2.png')}}"><div class="overview"></div></a> </li>
                        <li><a target="_blank" href="javascript:void(0);"><img src="{{asset('img/sponsor-aerodrom.png')}}"><div class="overview"></div></a> </li>
                        <li><a target="_blank" href="javascript:void(0);"><img src="{{asset('img/sponsor-tm-inzenjering.png')}}"><div class="overview"></div></a> </li>
                        <li><a target="_blank" href="javascript:void(0);"><img src="{{asset('img/sponsor-dunav.png')}}"><div class="overview"></div></a> </li>
                    </ul>
                    <h2 class="">МЕДИЈСКИ ПАРТНЕРИ</h2>
                    <ul>
                        <li><a target="_blank" href="javascript:void(0);"><img src="{{asset('img/sponsor-sd.png')}}"><div class="overview"></div></a> </li>
                        <li><a href="javascript:void(0);"><img src="{{asset('img/zona.png')}}"><div class="overview"></div></a> </li>
                        <li><a href="javascript:void(0);"><img src="{{asset('img/hotsport.png')}}"><div class="overview"></div></a> </li>
{{--                        <li><a target="_blank" href="javascript:void(0);"><img src="{{asset('img/sponsor-niskevesti.png')}}"><div class="overview"></div></a> </li>--}}
{{--                        <li><a target="_blank" href="javascript:void(0);"><img src="{{asset('img/sponsor-srbija-sport-net.png')}}"><div class="overview"></div></a> </li>--}}
                        <li><a target="_blank" href="javascript:void(0);"><img src="{{asset('img/sponsor-RTVBA.png')}}"><div class="overview"></div></a> </li>
{{--                        <li><a target="_blank" href="javascript:void(0);"><img src="{{asset('img/sponsor-juznasrbija.png')}}"><div class="overview"></div></a> </li>--}}
{{--                        <li><a target="_blank" href="javascript:void(0);"><img src="{{asset('img/sponsor-juzne-vesti.png')}}"><div class="overview"></div></a> </li>--}}
{{--                        <li><a target="_blank" href="javascript:void(0);"><img src="{{asset('img/sponsor-lniska-televizija.png')}}"><div class="overview"></div></a> </li>--}}
                        <li><a target="_blank" href="javascript:void(0);"><img src="{{asset('img/sponsor-narodne-novine.png')}}"><div class="overview"></div></a> </li>
                    </ul>
                    <h2 class="mb-5">СУПЕР ЛИГА СРБИЈЕ</h2>
                    <ul>
                        <li><a target="_blank" href="https://www.superliga.rs/"><img style="max-width: 300px" src="{{asset('img/superliga.png')}}"><div class="overview"></div></a> </li>
                    </ul>
                </section>
                @endif
            </main>
            <footer>
                <div class="container-big footer-content clearfix">
                    <div class="footer-block footer-block-one">
                        <h3 class="vertical-line">Тим</h3>
                        <ul>
                            <li><a href="{{ url('/first-team') }}">Први тим</a></li>
                            <li><a href="{{ url('/expert-staff') }}">Стручни штаб</a></li>
                            {{--<li><a href="{{ url('/sponsors') }}">Спонзори</a></li>--}}
                        </ul>
                    </div>
                    <div class="footer-block footer-block-two">
                        <h3 class="vertical-line">Вести</h3>
                        <ul>
                            <li><a href="{{ url('/news') }}">Све вести</a></li>
                            <li><a href="{{ url('/news') }}">Најновије вести</a></li>
                            <li><a href="{{ url('/reports') }}">Извештаји</a></li>
                        </ul>
                    </div>
                    <div class="footer-block footer-block-three">
                        <h3 class="vertical-line">О клубу</h3>
                        <ul>
                            <li><a href="{{ url('/club-history') }}">Историја клуба</a></li>
                            <li><a href="{{ url('/club-stars') }}">Звезде Радничког</a></li>
                        </ul>
                    </div>
                    <div class="footer-block footer-block-four">
                        <h3 class="vertical-line">Детаљније</h3>
                        <ul>
                            <li><a href="{{ url('/stadium') }}">Стадион</a></li>
                            <li><a href="{{ url('/our-fans') }}">Наши навијачи</a></li>
                            <li><a href="{{ url('/schedule-results') }}">Резултати и табела</a></li>
                        </ul>
                    </div>
                    <div class="footer-block footer-block-four">
                        <h3 class="vertical-line">Менаџмент</h3>
                        <ul>
                            <li><a href="{{ url('/managers') }}">Управа</a></li>
                            <li><a href="{{ url('/marketing') }}">Маркетинг</a></li>
                            <li><a href="{{ url('/sponsors') }}">Спонзори</a></li>
                        </ul>
                    </div>
                    <div class="footer-block footer-block-five">
                        <h3 class="vertical-line">Контакт</h3>
                        <p>Градски стадион Чаир</p>
                        <p>9. бригаде ББ 18000 Ниш</p>
                        <p>+381 18 527 259</p>
                        <p>office@fkradnickinis.rs</p>
                    </div>
                    <div class="footer-block footer-block-six">
                        <h3 class="vertical-line">Пратите нас</h3>
                        <ul>
                            <li><a href="{{url('https://www.facebook.com/RadnickiNisOfficial/?ref=br_rs')}}" target="_blank" class="footer-icon"><i class="fab fa-facebook"></i>Facebook</a></li>
                            <li><a href="{{url('https://www.instagram.com/fkradnickinis/?hl=sr')}}" target="_blank" class="footer-icon"><i class="fab fa-instagram"></i>Instagram</a></li>
                            <li><a href="{{url('https://plus.google.com/105547925413940230771')}}" target="_blank" class="footer-icon"><i class="fab fa-google-plus-g"></i>Google +</a></li>
                            <li><a href="{{url('https://twitter.com/fkradnickinisrs')}}" target="_blank" class="footer-icon"><i class="fab fa-twitter"></i>Twitter</a></li>
                            <li><a href="{{url('https://www.youtube.com/channel/UCZsRDmK2EHfsXQ7f6Urrf0A')}}" target="_blank" class="footer-icon"><i class="fab fa-youtube"></i>You Tube</a></li>
                        </ul>
                    </div>
                </div>
                <div class="copyright">
                    <p>Сва права задржава <span>ФК Раднички Ниш</span> © 2018.</p>
                </div>
            </footer>
            @endif
        </div>
    </div>
</body>
</html>
