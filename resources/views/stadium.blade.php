@extends('layouts.app')
{{--stadion--}}
@section('content')
    <header class="page-header stadion-header">
        <a href="{{url('/')}}">
            <div class="logo-small">
                <img src="{{asset('img/header-logo.png')}}">
                <p class="logo-text">ФК РАДНИЧКИ</p>
            </div>
        </a>
        <div class="menu-bar"><i class="fa fa-bars" aria-hidden="true"></i></div>
        <nav class="transp-red header-red">
            <div class="container-big clearfix">
                <ul class="menu-left">
                    <li><a href="{{url('/')}}" class="menu-link">Почетна</a> </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-link">О клубу</a>
                        <ul>
                            <li><a href="{{url('/club-history')}}" class="menu-link">Историја клуба</a></li>
                            <li><a href="{{url('/club-stars')}}" class="menu-link">Звезде радничког</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/news')}}" class="menu-link">Вести</a> </li>
                    <li><a href="{{url('/reports')}}" class="menu-link">Извештаји</a> </li>
                    <li>
                        <a href="{{url('/marketing')}}" class="menu-link">Маркетинг</a>
                        <ul>
                            <li><a href="{{url('/sponsors')}}" class="menu-link">Спонзори</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/managers')}}" class="menu-link">Управа клуба</a> </li>
                </ul>
                <a href="{{url('/')}}">
                    <div class="logo">
                        <img src="{{asset('img/header-logo.png')}}">
                        <p class="logo-text">ФК РАДНИЧКИ</p>
                    </div>
                </a>
                <ul class="menu-right">
                    <li><a href="{{url('/schedule-results')}}" class="menu-link">Резултати и распореди</a> </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-link">Тим</a>
                        <ul>
                            <li><a href="{{url('/first-team')}}" class="menu-link">Први тим</a></li>
                            <li><a href="{{url('/expert-staff')}}" class="menu-link">Стручни штаб</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/os-news')}}" class="menu-link">Омладинска школа</a></li>
                    <li><a href="{{url('/stadium')}}" class="menu-link active">Стадион</a> </li>
                    <li><a href="{{url('/our-fans')}}" class="menu-link">Наши навијачи</a> </li>
                </ul>
            </div>
        </nav>
    </header>

    <main class="page-margin-top-negative-big">
        <div class="container-big stadion">
            <h1 class="page-title">СТАДИОН<span>ЧАИР</span></h1>
            <div class="fans-content clearfix">
                <div class="stadion-chair clearfix">
                    <div class="fans-text">
                        <h2 class="fans-text-title">СТАДИОН ЧАИР</h2>
                        {{--<p class="fans-text-content"> У Спортском центру Чаир, после припремних и земљаних радова, започета је изградња бетонских стубова и конструкције источне, јужне и северне трибине. Тако ће бити постављен скелет за три трибине стадиона Чаир како би даље били настављени грађевински радови на изградњи.<br/>--}}
                            {{--Реконструкција стадиона ”Чаир” започета је радовима на источној трибини која није у функцији последњих 15 година. Главним пројектом предвиђена је реконструкција трибине на источној страни за 6,966 гледалаца. После источне следи реконструкција јужне и северне трибине које ће моћи да приме 9037 гледалаца, док ће западна трибина после радова бити отворена за 2154 навијача.--}}
                        {{--</p>--}}
                        <p class="fans-text-content">Домаћи терен Фудбалског клуба Раднички налази се у склопу градског спортског комплекса, недалеко од средишта града у издвојеном комплексу Чаир.
                        </p>
                        <p class="fans-text-content">Стадион на коме тренира и игра клуб изграђен је давне 1963. Око централног терена димензија 105м x 70м налази се атлетска стаза у дужини од 400м. Терен је окружен са свих страна трибинама са укупним капацитетом од преко 18.000 места, док је у току најава реконструкције истог, која би се огледала у покривању стадиона и повећању постојећег капацитета али и самој модернизацији сходно актуелним трендовима.
                        </p>
                        {{--<p class="fans-text-content">Стадион је изграђен 1963. године, а након реконструкције 2011-2013. у којој су изграђене три нове трибине (југ, исток и север) капацитет стадиона је 18.151 седећих места.--}}
                        {{--</p>--}}
                        {{--<p class="fans-text-content">Стадион је део Спортског комплекса Чаир.--}}
                        {{--</p>--}}
                    </div>
                    <div class="fans-imgs">
                        <img src="{{asset('img/new-images/stadium/slika1.jpg')}}">
                        <img src="{{asset('img/new-images/stadium/slika2.jpg')}}">
                        <img src="{{asset('img/new-images/stadium/slika3.jpg')}}">
                        <img src="{{asset('img/new-images/stadium/slika4.jpg')}}">
                        <img src="{{asset('img/new-images/stadium/slika5.jpg')}}">
                    </div>
                </div>
                <div class="stadion-map">
                    <h2 class="fans-text-title">ЛОКАЦИЈА СТАДИОНА</h2>
                    <div class="map" id="map"></div>
                </div>
                <div class="old-chair">
                    <div class="fans-text">
                        <h2 class="fans-text-title">СТАРИ СТАДИОН ЧАИР</h2>
                        <p class="fans-text-content">Као атрактиван ривал, ФК Раднички је од почетка ’50-их редовно био домаћин у пријатељским утакмицама екипама из иностранства. У Нишу су гостовали Макаби, Грац, АСК Клагенфурт, а поводом отварања стадиона Чаир, 28.септембра 1955. године уприличен је меч са екипом Ираклиса. Званично је пуштен у функцију 1963. године на месту где је некада било тркалиште, тада на периферији града. Градски стадион Чаир у Нишу је део великог комплекса спортског центра Чаир, налази се у парку Чаир и састоји се од централног терена и два помоћна терена.
                        </p>
                        <p class="fans-text-content">Централни терен је димензија 105м x 70м са атлетском тартан стазом укупне дужине 400м. Док је стари стадион био у функцији имао је 20,000 стајаћих места, касније је 2006. године постављено 3,000 столица за седење на западној трибини. Комплекс терена поседује и два помоћна терена поред стадиона један је са природном а други терен са вештачком травом.
                        </p>
                        <p class="fans-text-content">Дужи временски период свлачионице за фудбалере су биле испод источне трибине стадиона, али пробојем Радничког у Европу почетком осамдесетих година прошлог века, стадион је доживео прве веће реконструкције. Онда је покривена западна трибина, направљене ложе на западу, инсталиран модеран у то време разглас на западу од стране тадашњег гиганта ЕИ Ниш, МИН Ниш је урадио четири рефлектора који су и данас у функцији, а свлачионице и омладинска школа су испод јужне трибине. Почетком осамдесетих градски стадион Чаир у Нишу је био модеран стадион на ком су се играле утакмице купа УЕФА.
                        </p>
                        <p class="fans-text-content">Од тог старог стадиона који је последњих година тотално био руиниран и представљао ругло града, треба да никне велелепно здање новог стадиона који је пројектовао “Ниш Пројект” који ће представљати један од најмодернијих стадиона у земљи. Стари стадион је срушен почетком 2011. године, и многе Нишлије са сетом су гледале како под налетом багера падају трибине фудбалске позорнице која је угостила многа европска клупска имена. Стари Чаир одлази у историју и не смемо да дозволимо да се заборави шта се ту све десило и колико је среће и радости Раднички донео Нишлијама на старом Чаиру пуних 48 година.
                        </p>
                        <p class="fans-text-content">Нови стадион Чаир представља будућност за град, за Раднички, за нове генерације навијача и љубитеља фудбала у Нишу, али треба чувати успомену на оно што је било и писати нове странице историје.
                        </p>
                    </div>
                    <div class="fans-imgs">
                        <img src="{{asset('img/stadion05.jpg')}}">
                        <img src="{{asset('img/stadion06.jpg')}}">
                    </div>
                </div>
            </div>
        </div>
        <section class="tickets-apps clearfix">
            <div class="container-big">
                <div class="tickets">
                    <h2 class="horizontal-line-black">резервација карата</h2>
                    <div class="apps-content clearfix">
                        <div class="apps-text">
                            <h2 class="big wrap">резервишите
                                вашу
                                карту!</h2>
                            <p>Пoсетите сајт и резервишите карту за ваш омиљени клуб</p>
                            <a href="http://www.ticketline.rs/" target="_blank" class="tel hover-tel">Ticketline<i class="fas fa-arrow-right font-16 ml-3" style=""></i></a>
                        </div>
                        <img src="{{asset('img/sponsor-big-ticketline.png')}}" class="mob">
                    </div>
                </div>
                <div class="apps">
                    <h2 class="horizontal-line-black">званична брошура клуба</h2>
                    <div class="apps-content clearfix">
                        <div class="apps-text">
                            <h2 class="big wrap">преузмите
                                нашу званичну
                                брошуру</h2>
                            <p>Сазнајте информације о клубу!</p>
                            <a href="{{asset('/pdf/radnicki_katalog.pdf')}}" download class="tel brosura">Брошура<i class="fas fa-arrow-right font-16 ml-3" style=""></i></a>
                        </div>
                        <img src="{{asset('img/brosura.png')}}" class="mob">
                    </div>
                </div>
            </div>
        </section>
    </main>

    {{--MAIN SCRIPT--}}
    <script src="{{asset('js/main.js')}}"></script>
    
    {{--MAP--}}
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"
          integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ=="
          crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"
            integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw=="
            crossorigin=""></script>
    <script>
        var mymap = L.map('map').setView([43.315025, 21.908751], 16);

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1Ijoid3d3b2xmeSIsImEiOiJjamprYTFuY3IxdXl0M3dteGk3NWtpcDJ0In0.YuP8_vETG2mbAvQvlnZVnQ'
        }).addTo(mymap);
        var marker = L.marker([43.315025, 21.908751]).addTo(mymap);
    </script>
@endsection