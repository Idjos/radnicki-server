@extends('layouts.app')
{{--zvezde kluba--}}
@section('content')
    <header class="red-header">
        <a href="{{url('/')}}">
            <div class="logo-small">
                <img src="{{asset('img/header-logo.png')}}">
                <p class="logo-text">ФК РАДНИЧКИ</p>
            </div>
        </a>
        <div class="menu-bar"><i class="fa fa-bars" aria-hidden="true"></i></div>
        <nav class="red">
            <div class="container-big clearfix">
                <ul class="menu-left">
                    <li><a href="{{url('/')}}" class="menu-link ">Почетна</a> </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-link active">О клубу</a>
                        <ul>
                            <li><a href="{{url('/club-history')}}" class="menu-link">Историја клуба</a></li>
                            <li><a href="{{url('/club-stars')}}" class="menu-link">Звезде радничког</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/news')}}" class="menu-link">Вести</a> </li>
                    <li><a href="{{url('/reports')}}" class="menu-link">Извештаји</a> </li>
                    <li>
                        <a href="{{url('/marketing')}}" class="menu-link">Маркетинг</a>
                        <ul>
                            <li><a href="{{url('/sponsors')}}" class="menu-link">Спонзори</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/managers')}}" class="menu-link">Управа клуба</a> </li>
                </ul>
                <a href="{{url('/')}}">
                    <div class="logo">
                        <img src="{{asset('img/header-logo.png')}}">
                        <p class="logo-text">ФК РАДНИЧКИ</p>
                    </div>
                </a>
                <ul class="menu-right">
                    <li><a href="{{url('/schedule-results')}}" class="menu-link">Резултати и распореди</a> </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-link">Тим</a>
                        <ul>
                            <li><a href="{{url('/first-team')}}" class="menu-link">Први тим</a></li>
                            <li><a href="{{url('/expert-staff')}}" class="menu-link">Стручни штаб</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/os-news')}}" class="menu-link">Омладинска школа</a></li>
                    <li><a href="{{url('/stadium')}}" class="menu-link">Стадион</a> </li>
                    <li><a href="{{url('/our-fans')}}" class="menu-link">Наши навијачи</a> </li>
                </ul>
            </div>
        </nav>
    </header>

    <main class="margin-top">
        <section class="stars-radnickog clearfix">
            <h2 class="fans-text-title">ЗВЕЗДЕ РАДНИЧКОГ</h2>
            <div class="star">
                <a href="{{url('/miodrag-knezevic')}}">
                    <img class="star-img" src="{{asset('img/knezevic.jpg')}}">
                    <div class="star-radnicki-text clearfix">
                        <p class="star-number"></p>
                        <p class="star-name">Миодраг<span>Кнежевић</span></p>
                    </div>
                </a>
            </div>
            <div class="star">
                <a href="{{url('/dragan-holcer')}}">
                    <img class="star-img" src="{{asset('img/holcer.jpg')}}">
                    <div class="star-radnicki-text clearfix">
                        <p class="star-number"></p>
                        <p class="star-name">Драган<span>Холцер</span></p>
                    </div>
                </a>
            </div>
            <div class="star">
                <a href="{{url('/dusan-mitosevic')}}">
                    <img class="star-img" src="{{asset('img/mitosevic.jpg')}}">
                    <div class="star-radnicki-text clearfix">
                        <p class="star-number"></p>
                        <p class="star-name">Душан<span>Митошевић</span></p>
                    </div>
                </a>
            </div>
            <div class="star">
                <a href="{{url('/dragan-pantelic')}}">
                    <img class="star-img" src="{{asset('img/pantelic.jpg')}}">
                    <div class="star-radnicki-text clearfix">
                        <p class="star-number"></p>
                        <p class="star-name">Драган<span>Пантелић</span></p>
                    </div>
                </a>
            </div>
            <div class="star">
                <a href="{{url('/drda-obradovic')}}">
                    <img class="star-img" src="{{asset('img/drda.jpg')}}">
                    <div class="star-radnicki-text clearfix">
                        <p class="star-number"></p>
                        <p class="star-name">Милован Дрда<span>Обрадовић</span></p>
                    </div>
                </a>
            </div>
            <div class="star">
                <a href="{{url('/dragan-stojkovic')}}">
                    <img class="star-img" src="{{asset('img/piksi.jpg')}}">
                    <div class="star-radnicki-text clearfix">
                        <p class="star-number"></p>
                        <p class="star-name">Драган Пикси<span>Стојковић</span></p>
                    </div>
                </a>
            </div>
            <div class="star">
                <a href="{{url('/rambo-petkovic')}}">
                    <img class="star-img" src="{{asset('img/rambo.jpg')}}">
                    <div class="star-radnicki-text clearfix">
                        <p class="star-number"></p>
                        <p class="star-name">Дејан Рамбо<span>Петковић</span></p>
                    </div>
                </a>
            </div>
        </section>
        <section class="tickets-apps clearfix">
            <div class="container-big">
                <div class="tickets">
                    <h2 class="horizontal-line-black">резервација карата</h2>
                    <div class="apps-content clearfix">
                        <div class="apps-text">
                            <h2 class="big wrap">резервишите
                                вашу
                                карту!</h2>
                            <p>Пoсетите сајт и резервишите карту за ваш омиљени клуб</p>
                            <a href="http://www.ticketline.rs/" target="_blank" class="tel hover-tel">Ticketline<i class="fas fa-arrow-right font-16 ml-3" style=""></i></a>
                        </div>
                        <img src="{{asset('img/sponsor-big-ticketline.png')}}" class="mob">
                    </div>
                </div>
                <div class="apps">
                    <h2 class="horizontal-line-black">званична брошура клуба</h2>
                    <div class="apps-content clearfix">
                        <div class="apps-text">
                            <h2 class="big wrap">преузмите
                                нашу званичну
                                брошуру</h2>
                            <p>Сазнајте информације о клубу!</p>
                            <a href="{{asset('/pdf/radnicki_katalog.pdf')}}" download class="tel brosura">Брошура<i class="fas fa-arrow-right font-16 ml-3" style=""></i></a>
                        </div>
                        <img src="{{asset('img/brosura.png')}}" class="mob">
                    </div>
                </div>
            </div>
        </section>
    </main>

    {{--MAIN SCRIPT--}}
    <script src="{{asset('js/main.js')}}"></script>
@endsection