@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row d-flex justify-content-center m-0">
        <div class="col-12 col-md-6 col-lg-4">
            <div class="panel panel-default text-center login-form">
                <a href="{{ url('/') }}">
                    <img style="width: 100px" src="img/logo.png">
                </a>
                <div class="panel-body mt-4">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="">
                                <input id="email" type="email" class="form-control" name="email" placeholder="example@demo.com" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="">
                                <input id="password" type="password" class="form-control" name="password" placeholder="*********" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="">
                                <button type="submit" class="btn btn-danger w-100">
                                    Пријави се
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
