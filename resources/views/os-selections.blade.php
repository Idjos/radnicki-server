@extends('layouts.app')
{{--os selekcije--}}
@section('content')
    <header class="page-header os-news-all">
        <div class="logo-small">
            <img src="{{asset('img/header-logo.png')}}">
            <p class="logo-text">ФК РАДНИЧКИ</p>
        </div>
        <div class="menu-bar"><i class="fa fa-bars" aria-hidden="true"></i></div>
        <nav class="transp-red header-red">
            <div class="container-big clearfix">
                <ul class="menu-left">
                    <li><a href="{{url('/')}}" class="menu-link">Почетна</a> </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-link">О клубу</a>
                        <ul>
                            <li><a href="{{url('/club-history')}}" class="menu-link">Историја клуба</a></li>
                            <li><a href="{{url('/club-stars')}}" class="menu-link">Звезде радничког</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/news')}}" class="menu-link">Вести</a> </li>
                    <li><a href="{{url('/reports')}}" class="menu-link">Извештаји</a> </li>
                    <li>
                        <a href="{{url('/marketing')}}" class="menu-link">Маркетинг</a>
                        <ul>
                            <li><a href="{{url('/sponsors')}}" class="menu-link">Спонзори</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/managers')}}" class="menu-link">Управа клуба</a> </li>
                </ul>
                <a href="{{url('/')}}">
                    <div class="logo">
                        <img src="{{asset('img/header-logo.png')}}">
                        <p class="logo-text">ФК РАДНИЧКИ</p>
                    </div>
                </a>
                <ul class="menu-right">
                    <li><a href="{{url('/schedule-results')}}" class="menu-link">Резултати и распореди</a> </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-link">Тим</a>
                        <ul>
                            <li><a href="{{url('/first-team')}}" class="menu-link">Први тим</a></li>
                            <li><a href="{{url('/expert-staff')}}" class="menu-link">Стручни штаб</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/os-news')}}" class="menu-link active">Омладинска школа</a></li>
                    <li><a href="{{url('/stadium')}}" class="menu-link">Стадион</a> </li>
                    <li><a href="{{url('/our-fans')}}" class="menu-link">Наши навијачи</a> </li>
                </ul>
            </div>
        </nav>
    </header>

    <main class="page-margin-top-negative os-news">
        <div class="container-big os-news-header">
            <h1 class="page-title">ОМЛАДИНСКА
                <span>ШКОЛА</span></h1>
            <ul class="tim-navigation">
                <li><a href="{{url('/os-news')}}" class="tab-link">Вести</a> </li>
                <li><a href="{{url('/os-expert-staff')}}" class="tab-link">Стручни штаб</a> </li>
                <li><a href="{{url('/os-selections')}}" class="tab-link active">Селекције</a> </li>

            </ul>
        </div>
        <section class="container-big selections">
            <ul class="tim-navigation">
                @foreach($selections as $key => $selection)
                    <li><a style="font-size: 13px" href="javascript:void(0);" id="{{$selection->name}}" class="tab-link @if($key == 0) active @endif">{{str_limit($selection->name,20)}}</a> </li>
                @endforeach
            </ul>
            <!--Selekcije ubaci iz baze, ovaj data set isto ubaci dinamicki kao promenljivu ali nek ostanu ovi nazivi jer sluze za navigaciju.-->
            @foreach($selections as $key => $selection)
            <div class="selection @if($key == 0) active @endif" data-set="{{$selection->name}}">
                <h2 class="fans-text-title">{{$selection->name}}</h2>  <!--naslov-->
                <img src="{{asset('uploads/'.$selection->path)}}">  <!--slika-->
                <p class="selection-text">За нашу омладинску селекцију наступају:</p>
                <p class="selection-text-players">{{$selection->players}}</p> <!--lista igraca-->
                <p class="selection-text-players font-weight-bold">Утакмице:</p>
                <p class="selection-text">{!! $selection->matches !!}</p><!--utakmice-->
            </div>
            @endforeach
        </section>
        <section class="tickets-apps clearfix">
            <div class="container-big">
                <div class="tickets">
                    <h2 class="horizontal-line-black">резервација карата</h2>
                    <div class="apps-content clearfix">
                        <div class="apps-text">
                            <h2 class="big wrap">резервишите
                                вашу
                                карту!</h2>
                            <p>Пoсетите сајт и резервишите карту за ваш омиљени клуб</p>
                            <a href="http://www.ticketline.rs/" target="_blank" class="tel hover-tel">Ticketline<i class="fas fa-arrow-right font-16 ml-3" style=""></i></a>
                        </div>
                        <img src="{{asset('img/sponsor-big-ticketline.png')}}" class="mob">
                    </div>
                </div>
                <div class="apps">
                    <h2 class="horizontal-line-black">званична брошура клуба</h2>
                    <div class="apps-content clearfix">
                        <div class="apps-text">
                            <h2 class="big wrap">преузмите
                                нашу званичну
                                брошуру</h2>
                            <p>Сазнајте информације о клубу!</p>
                            <a href="{{asset('/pdf/radnicki_katalog.pdf')}}" download class="tel brosura">Брошура<i class="fas fa-arrow-right font-16 ml-3" style=""></i></a>
                        </div>
                        <img src="{{asset('img/brosura.png')}}" class="mob">
                    </div>
                </div>
            </div>
        </section>
    </main>

    {{--MAIN SCRIPT--}}
    <script src="{{asset('js/main.js')}}"></script>
@endsection