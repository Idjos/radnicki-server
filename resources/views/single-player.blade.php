@extends('layouts.app')
{{--Prvi tim single view--}}
@section('content')
    <header class="page-big-header player-single">
        <div style="background-image: url('{{asset('uploads/'.$member->path)}}');" class="img-player-big single-player">

        </div>
        <!-- <img class="img-player-big" src="{{asset('uploads/'.$member->path)}}"> -->
        {{--<img src="{{asset('img/header-player-small.jpg')}}" class="img-player-small">--}}
        <a href="{{url('/')}}">
            <div class="logo-small">
                <img src="{{asset('img/header-logo.png')}}">
                <p class="logo-text">ФК РАДНИЧКИ</p>
            </div>
        </a>
        <div class="menu-bar"><i class="fa fa-bars" aria-hidden="true"></i></div>
        <nav class="transp-red header-red">
            <div class="container-big clearfix">
                <ul class="menu-left">
                    <li><a href="{{url('/')}}" class="menu-link">Почетна</a> </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-link">О клубу</a>
                        <ul>
                            <li><a href="{{url('/club-history')}}" class="menu-link">Историја клуба</a></li>
                            <li><a href="{{url('/club-stars')}}" class="menu-link">Звезде радничког</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/news')}}" class="menu-link">Вести</a> </li>
                    <li><a href="{{url('/reports')}}" class="menu-link">Извештаји</a> </li>
                    <li>
                        <a href="{{url('/marketing')}}" class="menu-link">Маркетинг</a>
                        <ul>
                            <li><a href="{{url('/sponsors')}}" class="menu-link">Спонзори</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/managers')}}" class="menu-link">Управа клуба</a> </li>
                </ul>
                <a href="{{url('/')}}">
                    <div class="logo">
                        <img src="{{asset('img/header-logo.png')}}">
                        <p class="logo-text">ФК РАДНИЧКИ</p>
                    </div>
                </a>
                <ul class="menu-right">
                    <li><a href="{{url('/schedule-results')}}" class="menu-link">Резултати и распореди</a> </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-link active">Тим</a>
                        <ul>
                            <li><a href="{{url('/first-team')}}" class="menu-link">Први тим</a></li>
                            <li><a href="{{url('/expert-staff')}}" class="menu-link">Стручни штаб</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/os-news')}}" class="menu-link">Омладинска школа</a></li>
                    <li><a href="{{url('/stadium')}}" class="menu-link">Стадион</a> </li>
                    <li><a href="{{url('/our-fans')}}" class="menu-link">Наши навијачи</a> </li>
                </ul>
            </div>
        </nav>
    </header>
    <main class="page-margin-top-negative-player">
        <div class="container-big player-single-content">
            <div class="bio clearfix">
                <!--podaci o igracu broj ime i prezime ubacuj u spanove-->

                <h1 class="page-title"><span style="font-size: 60px" class="last-name">{{$member->number}}</span><span class="name">{{$member->name}}</span><span class="last-name">{{$member->lastname}}</span></h1>
                <div class="player-bio">
                    <img class="field-position"
                         @if($member->position == 'ГОЛМАН')
                         src="{{asset('img/golman.png')}}"
                         @elseif($member->position == 'ОДБРАНА')
                         src="{{asset('img/odbrana.png')}}"
                         @elseif($member->position == 'СРЕДИНА')
                         src="{{asset('img/sredina.png')}}"
                         @elseif($member->position == 'НАПАД')
                         src="{{asset('img/napad.png')}}"
                         @endif
                    ><!--slika u zavisnosti od pozicije, napad odbrana sredina golman-->
                    <p class="player-info"><span>Националност:</span><img class="flag" src="{{asset('img/flags/'.$member->country)}}"></p> <!--slika zastava-->
                    <p class="player-info"><span>Позиција:</span>{{$member->position}}</p><!--pozicija-->
                    <p class="player-info"><span>Број дреса:</span>{{$member->number}}</p><!--broj na dresu-->
                    <p class="player-info"><span>Датум рођења:</span>{{$member->birthday}}</p> <!--datum rodjenja-->
                    <p class="player-info"><span>Место рођења:</span>{{$member->place_of_birth}}</p><!--grad-->
                    <p class="player-info"><span>Каријера:</span> {{$member->desc}}</p><!--array prethodnih klubova-->
                </div>
            </div>
            <div class="list-players clearfix">
                <!--array igraca po poziciji broj ime i preyime ujedno link ka single view-->
                <h2 class="horizontal-line-black">ИГРАЧИ</h2>
                <ul>
                    <h3 class="title-position">Голман</h3>
                    @foreach($players as $player)
                        @if($player->position == 'ГОЛМАН')
                        <li><a href="{{asset('single-player/'.$player->id)}}" @if($player->id == $member->id) class="active" @endif>{{$player->name}} {{$player->lastname}}</a></li>
                        @endif
                    @endforeach
                </ul>
                <ul>
                    <h3 class="title-position">Одбрана</h3>
                    @foreach($players as $player)
                        @if($player->position == 'ОДБРАНА')
                            <li><a href="{{asset('single-player/'.$player->id)}}" @if($player->id == $member->id) class="active" @endif>{{$player->name}} {{$player->lastname}}</a></li>
                        @endif
                    @endforeach
                </ul>
                <ul>
                    <h3 class="title-position">Средина</h3>
                    @foreach($players as $player)
                        @if($player->position == 'СРЕДИНА')
                            <li><a href="{{asset('single-player/'.$player->id)}}" @if($player->id == $member->id) class="active" @endif>{{$player->name}} {{$player->lastname}}</a></li>
                        @endif
                    @endforeach
                </ul>
                <ul>
                    <h3 class="title-position">Напад</h3>
                    @foreach($players as $player)
                        @if($player->position == 'НАПАД')
                            <li><a href="{{asset('single-player/'.$player->id)}}" @if($player->id == $member->id) class="active" @endif>{{$player->name}} {{$player->lastname}}</a></li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
        <section class="tickets-apps clearfix">
            <div class="container-big">
                <div class="tickets">
                    <h2 class="horizontal-line-black">резервација карата</h2>
                    <div class="apps-content clearfix">
                        <div class="apps-text">
                            <h2 class="big wrap">резервишите
                                вашу
                                карту!</h2>
                            <p>Пoсетите сајт и резервишите карту за ваш омиљени клуб</p>
                            <a href="http://www.ticketline.rs/" target="_blank" class="tel hover-tel">Ticketline<i class="fas fa-arrow-right font-16 ml-3" style=""></i></a>
                        </div>
                        <img src="{{asset('img/sponsor-big-ticketline.png')}}" class="mob">
                    </div>
                </div>
                <div class="apps">
                    <h2 class="horizontal-line-black">званична брошура клуба</h2>
                    <div class="apps-content clearfix">
                        <div class="apps-text">
                            <h2 class="big wrap">преузмите
                                нашу званичну
                                брошуру</h2>
                            <p>Сазнајте информације о клубу!</p>
                            <a href="{{asset('/pdf/radnicki_katalog.pdf')}}" download class="tel brosura">Брошура<i class="fas fa-arrow-right font-16 ml-3" style=""></i></a>
                        </div>
                        <img src="{{asset('img/brosura.png')}}" class="mob">
                    </div>
                </div>
            </div>
        </section>
    </main>

    {{--MAIN SCRIPT--}}
    <script src="{{asset('js/main.js')}}"></script>
@endsection
