@extends('layouts.app')
{{--novosti single view--}}
@section('content')
    {{--<style>--}}
        {{--.container-small img {--}}
            {{--width: ;--}}
        {{--}--}}
    {{--</style>--}}
    <header class="news-single-header">
        <div style="background-image: url('{{asset('uploads/'.$news->path)}}');" class="single-image">
            
        </div>
        <!-- <img src="{{asset('uploads/'.$news->path)}}"> -->
        <a href="{{url('/')}}">
            <div class="logo-small">
                <img src="{{asset('img/header-logo.png')}}">
                <p class="logo-text">ФК РАДНИЧКИ</p>
            </div>
        </a>
        <div class="menu-bar"><i class="fa fa-bars" aria-hidden="true"></i></div>
        <nav class="transp-red header-red">
            <div class="container-big clearfix">
                <ul class="menu-left">
                    <li><a href="{{url('/')}}" class="menu-link">Почетна</a> </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-link">О клубу</a>
                        <ul>
                            <li><a href="{{url('/club-history')}}" class="menu-link">Историја клуба</a></li>
                            <li><a href="{{url('/club-stars')}}" class="menu-link">Звезде радничког</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/news')}}" class="menu-link active">Вести</a> </li>
                    <li><a href="{{url('/reports')}}" class="menu-link">Извештаји</a> </li>
                    <li>
                        <a href="{{url('/marketing')}}" class="menu-link">Маркетинг</a>
                        <ul>
                            <li><a href="{{url('/sponsors')}}" class="menu-link">Спонзори</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/managers')}}" class="menu-link">Управа клуба</a> </li>
                </ul>
                <a href="{{url('/')}}">
                    <div class="logo">
                        <img src="{{asset('img/header-logo.png')}}">
                        <p class="logo-text">ФК РАДНИЧКИ</p>
                    </div>
                </a>
                <ul class="menu-right">
                    <li><a href="{{url('/schedule-results')}}" class="menu-link">Резултати и распореди</a> </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-link">Тим</a>
                        <ul>
                            <li><a href="{{url('/first-team')}}" class="menu-link">Први тим</a></li>
                            <li><a href="{{url('/expert-staff')}}" class="menu-link">Стручни штаб</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/os-news')}}" class="menu-link">Омладинска школа</a></li>
                    <li><a href="{{url('/stadium')}}" class="menu-link">Стадион</a> </li>
                    <li><a href="{{url('/our-fans')}}" class="menu-link">Наши навијачи</a> </li>
                </ul>
            </div>
        </nav>
        <a href="javascript:void(0);" class="go-down" id="trigger-news"><img src="{{asset('img/go-down.png')}}"></a>
    </header>

    <main>
        <section class="news-main" id="news-main">
            <div class="container-with-tiny">

                <!--datum-->
                <p class="news-date">{{date('d.m.Y', strtotime($news->created_at))}}</p>
                <h1 class="news-title">{{$news->name}}</h1><!--naslov-->
                <p class="news-category">{{$news->type}}</p><!--kategorija-->

                <!--text-->
                <p>{!! $news->desc !!}</p>
            </div>
            <div class="comments-container clearfix">
                <h2 class="horizontal-line-black">КОМЕНТАРИ <span>{{count($news->comments)}}</span></h2>
                <div class="comments-author-input">
                    <div class="image-frame">
                        <img src="{{asset('img/comment-author.png')}}">
                    </div>
                </div>
                <div class="form">
                    <form class="comments-form" action="{{asset('/comment')}}" method="POST">
                        {!! csrf_field() !!}
                        <input hidden name="latest_new_id" value="{{$news->id}}">
                        <input id="firstname" type="text" name="name" placeholder="Име и презиме" required>
                        <input id="email" type="email" name="email" placeholder="Емаил адреса" required>
                        <textarea id="comments" name="desc" placeholder="Коментар..." required></textarea>
                        <p>Сва поља су обавезна.</p>
                        <input class="button button-red" type="submit" value="додајте коментар">
                    </form>
                </div>
            </div>
            <div class="comments-container comments-output">
                @foreach($news->comments as $comment)
                <div class="comment clearfix">
                    <div class="comments-author-input">
                        <div class="image-frame">
                            <img src="{{asset('img/comment-author.png')}}">
                        </div>
                    </div>
                    <div class="comment-text">
                        <p class="comment-author">{{$comment->name}}</p>
                        <p class="comment-content">{{$comment->desc}}</p>
                    </div>
                    @auth
                    <div class="float-right pb-2">
                        <form method="GET" action="{{asset('admin/delete_comment/'.$comment->id)}}">
                            {!! csrf_field() !!}
                            <button type="submit" class="btn btn-outline-danger float-right">Обриши</button>
                        </form>
                    </div>
                    @endauth
                </div>

                @endforeach
            </div>
        </section>
        <section class="tickets-apps clearfix">
            <div class="container-big">
                <div class="tickets">
                    <h2 class="horizontal-line-black">резервација карата</h2>
                    <div class="apps-content clearfix">
                        <div class="apps-text">
                            <h2 class="big wrap">резервишите
                                вашу
                                карту!</h2>
                            <p>Пoсетите сајт и резервишите карту за ваш омиљени клуб</p>
                            <a href="http://www.ticketline.rs/" target="_blank" class="tel hover-tel">Ticketline<i class="fas fa-arrow-right font-16 ml-3" style=""></i></a>
                        </div>
                        <img src="{{asset('img/sponsor-big-ticketline.png')}}" class="mob">
                    </div>
                </div>
                <div class="apps">
                    <h2 class="horizontal-line-black">званична брошура клуба</h2>
                    <div class="apps-content clearfix">
                        <div class="apps-text">
                            <h2 class="big wrap">преузмите
                                нашу званичну
                                брошуру</h2>
                            <p>Сазнајте информације о клубу!</p>
                            <a href="{{asset('/pdf/radnicki_katalog.pdf')}}" download class="tel brosura">Брошура<i class="fas fa-arrow-right font-16 ml-3" style=""></i></a>
                        </div>
                        <img src="{{asset('img/brosura.png')}}" class="mob">
                    </div>
                </div>
            </div>
        </section>
    </main>

    {{--MAIN SCRIPT--}}
    <script src="{{asset('js/main.js')}}"></script>
@endsection