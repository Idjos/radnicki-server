@extends('layouts.app')
{{--novosti--}}
@section('content')
    <header class="page-header news-all">
        <a href="{{url('/')}}">
            <div class="logo-small">
                <img src="{{asset('img/header-logo.png')}}">
                <p class="logo-text">ФК РАДНИЧКИ</p>
            </div>
        </a>
        <div class="menu-bar"><i class="fa fa-bars" aria-hidden="true"></i></div>
        <nav class="transp-red header-red">
            <div class="container-big clearfix">
                <ul class="menu-left">
                    <li><a href="{{url('/')}}" class="menu-link">Почетна</a> </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-link">О клубу</a>
                        <ul>
                            <li><a href="{{url('/club-history')}}" class="menu-link">Историја клуба</a></li>
                            <li><a href="{{url('/club-stars')}}" class="menu-link">Звезде радничког</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/news')}}" class="menu-link active">Вести</a> </li>
                    <li><a href="{{url('/reports')}}" class="menu-link">Извештаји</a> </li>
                    <li>
                        <a href="{{url('/marketing')}}" class="menu-link">Маркетинг</a>
                        <ul>
                            <li><a href="{{url('/sponsors')}}" class="menu-link">Спонзори</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/managers')}}" class="menu-link">Управа клуба</a> </li>
                </ul>
                <a href="{{url('/')}}">
                    <div class="logo">
                        <img src="{{asset('img/header-logo.png')}}">
                        <p class="logo-text">ФК РАДНИЧКИ</p>
                    </div>
                </a>
                <ul class="menu-right">
                    <li><a href="{{url('/schedule-results')}}" class="menu-link">Резултати и распореди</a> </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-link">Тим</a>
                        <ul>
                            <li><a href="{{url('/first-team')}}" class="menu-link">Први тим</a></li>
                            <li><a href="{{url('/expert-staff')}}" class="menu-link">Стручни штаб</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('/os-news')}}" class="menu-link">Омладинска школа</a></li>
                    <li><a href="{{url('/stadium')}}" class="menu-link">Стадион</a> </li>
                    <li><a href="{{url('/our-fans')}}" class="menu-link">Наши навијачи</a> </li>
                </ul>
            </div>
        </nav>
    </header>

    <main class="page-margin-top-negative">
        <div class="container-big">
            <h1 class="page-title">НАЈНОВИЈЕ ВЕСТИ
                {{--<span>ФК РАДНИЧКИ НИШ</span>--}}
            </h1>
        </div>
        <section class="news-events clearfix">
            <div class="container-big background clearfix">
                <div class="all-news">
                    <!--vesti-->

                    <!--u newsrow po dve vesti-->
                    @foreach($news as $key => $new)
                        @if($key != 2)
                            <div class="news-row">
                                <a href="{{asset('news-single-view/'.$new->id)}}">
                                    <div class="news">
                                        <div style="background-image: url('{{asset('uploads/'.$new->path)}}');" class="img-homepage"></div>
                                    <!-- <img src="{{asset('uploads/'.$new->path)}}" class="news-img"> <!--featured img za vest--> -->
                                        <div class="news-content p-4">
                                            <p class="category-news category-news-red">{{$new->type}}</p><!--kategorija-->
                                            <a href="{{asset('news-single-view/'.$new->id)}}" class="news-link"><h2>{{$new->name}}</h2></a><!--naslov ujedno i link ka single view-->
                                            <p>{!! strip_tags(str_limit($new->desc,100),  '<p><br>') !!}</p> <!--excerpt vesti -->
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @else
                            <a href="{{asset('news-single-view/'.$new->id)}}">
                                <div class="news news-important mr-4">
                                    {{--<img src="{{asset('uploads/'.$new->path)}}" class="news-img">--}}
                                    <div style="background-image: url('{{asset('uploads/'.$new->path)}}');" class="img-homepage"></div>
                                    <div class="news-content news-content-important">
                                        <p class="category-news category-news-red">{{$new->type}}</p>
                                        <a href="{{asset('news-single-view/'.$new->id)}}" class="news-link"><h2>{{str_limit($new->name,60)}}</h2></a>
{{--                                        <p>{!! strip_tags(str_limit($new->desc,100),  '<p><br>') !!}</p>--}}
                                    </div>
                                </div>
                            </a>
                        @endif
                    @endforeach
                </div>
                <div class="events" style="padding: 20px; background-color: #fff; @if(count($events) == 0) display: none; @endif">
                    <div class="row news-blue">
                        @foreach($events as $event)
                            <div class="col-12">
                                <div class="blue-news">
                                    <h3>{{date('d', strtotime($event->date))}}</h3>
                                    <h5>{{date('M', strtotime($event->date))}}</h5>
                                </div>
                                <div class="text-news">
                                    <h4>{{$event->name}}</h4>
                                    <h6>{{$event->desc}}</h6>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="container-big">
                <div class="custom-pagination d-flex justify-content-center">
                    {{ $news->links('vendor.pagination.simple-default') }}
                </div>
            </div>
        </section>
        <section class="tickets-apps clearfix">
            <div class="container-big">
                <div class="tickets">
                    <h2 class="horizontal-line-black">резервација карата</h2>
                    <div class="apps-content clearfix">
                        <div class="apps-text">
                            <h2 class="big wrap">резервишите
                                вашу
                                карту!</h2>
                            <p>Пoсетите сајт и резервишите карту за ваш омиљени клуб</p>
                            <a href="http://www.ticketline.rs/" target="_blank" class="tel hover-tel">Ticketline<i class="fas fa-arrow-right font-16 ml-3" style=""></i></a>
                        </div>
                        <img src="{{asset('img/sponsor-big-ticketline.png')}}" class="mob">
                    </div>
                </div>
                <div class="apps">
                    <h2 class="horizontal-line-black">званична брошура клуба</h2>
                    <div class="apps-content clearfix">
                        <div class="apps-text">
                            <h2 class="big wrap">преузмите
                                нашу званичну
                                брошуру</h2>
                            <p>Сазнајте информације о клубу!</p>
                            <a href="{{asset('/pdf/radnicki_katalog.pdf')}}" download class="tel brosura">Брошура<i class="fas fa-arrow-right font-16 ml-3" style=""></i></a>
                        </div>
                        <img src="{{asset('img/brosura.png')}}" class="mob">
                    </div>
                </div>
            </div>
        </section>
    </main>

    {{--MAIN SCRIPT--}}
    <script src="{{asset('js/main.js')}}"></script>
@endsection