@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mb-md-5 mb-5">
            <div class="col-12 col-sm-8">
                <h2>Играчи</h2>
            </div>
            <div class="col-12 col-sm-4 text-left text-sm-right">
                <button type="button" class="btn btn-outline-dark" data-toggle="modal" data-target="#addNew">Додај играча</button>
            </div>
        </div>
        <div class="row d-flex">
            @if(count($members) == null)
                <div class="col-12 text-center">
                    <h2>Нема резултата</h2>
                </div>
            @endif
            @foreach($members as $member)
                <div class="col-12 col-md-4 justify-content-center mb-4">
                    <div class="card custom-card" style="width: 18rem;">
                        <div class="image-container text-center">
                            <img class="card-img-top" src="{{asset('uploads/'.$member->path)}}">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">{{$member->name}} {{$member->lastname}}</h5>
                            <p class="card-text grey-font font-14">{{$member->position}}</p>
                            <form action="{{asset('admin/delete_member/'.$member->id)}}" method="GET">
                                <button data-toggle="modal" data-target="#editPlayer{{$member->id}}" type="button" class="btn btn-outline-info">Измени</button>
                                <button type="submit" class="btn btn-outline-danger">Обриши</button>
                            </form>
                        </div>
                    </div>
                </div>
                    <!-- add new -->
                    <div class="modal fade" id="editPlayer{{$member->id}}" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <form action="member/{{$member->id}}" method="POST" enctype="multipart/form-data">
                                    {!! csrf_field() !!}
                                    <div class="modal-header">
                                        <h5 class="modal-title">Измени играча</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body mt-3">
                                        <input type="file" name="avatar" accept="image/x-png,image/gif,image/jpeg"/><br>
                                        <img style="width: 200px;" src="{{asset('uploads/'.$member->path)}}">
                                        <div class="form-group mt-2">
                                            <input type="text" class="form-control" placeholder="Име" value="{{$member->name}}" name="name" required>
                                        </div>
                                        <div class="form-group mt-2">
                                            <input type="text" class="form-control" placeholder="Презиме" value="{{$member->lastname}}" name="lastname" required>
                                        </div>
                                        <div class="form-group mt-2">
                                            <input type="number" class="form-control" placeholder="Број" value="{{$member->number}}" name="number" required>
                                        </div>
                                        <div class="form-group mt-2">
                                            <input type="date" class="form-control" placeholder="Датум рођења" value="{{$member->birthday}}" name="birthday" required>
                                        </div>
                                        <div class="form-group mt-2">
                                            <input type="text" class="form-control" placeholder="Место рођења" value="{{$member->place_of_birth}}" name="place_of_birth" required>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control" name="position" required>
                                                <option value="" disabled hidden selected>Позиција</option>
                                                <option @if($member->position == 'ГОЛМАН'){{'selected'}}@endif>ГОЛМАН</option>
                                                <option @if($member->position == 'ОДБРАНА'){{'selected'}}@endif>ОДБРАНА</option>
                                                <option @if($member->position == 'СРЕДИНА'){{'selected'}}@endif>СРЕДИНА</option>
                                                <option @if($member->position == 'НАПАД'){{'selected'}}@endif>НАПАД</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <select class="form-control" name="country" required>
                                                <option value="" disabled hidden selected>Земља</option>
                                                @foreach (File::allFiles(public_path('img/flags')) as $filename)
                                                    <option @if($member->country == $filename->getFilename()){{'selected'}}@endif value="{{$filename->getFilename()}}">
                                                        <?php
                                                        $splitName = explode('-', $filename->getFilename(), 2);
                                                        $name = explode('.', $splitName[1], 2);
                                                        ?>

                                                        <span>{{ucfirst($name[0])}}</span>
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" name="desc" rows="10" placeholder="Опис" required>{{$member->desc}}</textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-danger">Сачувај</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
            @endforeach
        </div>
    </div>

    <!-- add new -->
    <div class="modal fade" id="addNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="member" method="POST" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Додај играча</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mt-3">
                        <input type="file" name="avatar" accept="image/x-png,image/gif,image/jpeg" required/>
                        <div class="form-group mt-2">
                            <input type="text" class="form-control" placeholder="Име" name="name" required>
                        </div>
                        <div class="form-group mt-2">
                            <input type="text" class="form-control" placeholder="Презиме" name="lastname" required>
                        </div>
                        <div class="form-group mt-2">
                            <input type="number" class="form-control" placeholder="Број" name="number" required>
                        </div>
                        <div class="form-group mt-2">
                            <input type="date" class="form-control" placeholder="Датум рођења" name="birthday" required>
                        </div>
                        <div class="form-group mt-2">
                            <input type="text" class="form-control" placeholder="Место рођења" name="place_of_birth" required>
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="position" required>
                                <option value="" disabled hidden selected>Позиција</option>
                                <option>ГОЛМАН</option>
                                <option>ОДБРАНА</option>
                                <option>СРЕДИНА</option>
                                <option>НАПАД</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <select class="form-control" name="country" required>
                                <option value="" disabled hidden selected>Земља</option>
                                @foreach (File::allFiles(public_path('img/flags')) as $filename)
                                    <option value="{{$filename->getFilename()}}">
                                        <?php
                                        $splitName = explode('-', $filename->getFilename(), 2);
                                        $name = explode('.', $splitName[1], 2);
                                        ?>

                                        <span>{{ucfirst($name[0])}}</span>
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="desc" rows="10" placeholder="Опис" required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">Сачувај</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
