@extends('layouts.app')

@section('content')
    <div class="container">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs admin-pills">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#news">Вести</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#experts">Стручни штаб</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">Селекције</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item font-weight-bold" data-toggle="tab" href="#new-selection">Додај нову селекцију</a>
                    @foreach($selections as $selection)
                    <a class="dropdown-item" data-toggle="tab" href="#selection{{$selection->id}}">{{str_limit($selection->name,60)}}</a>
                    @endforeach
                </div>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            {{--NEWS--}}
            <div class="tab-pane container active" id="news">
                <div class="row mt-5 mb-md-5 mb-5">
                    <div class="col-12 col-sm-8">
                        <h2>Новости</h2>
                    </div>
                    <div class="col-12 col-sm-4 text-left text-sm-right">
                        <button type="button" class="btn btn-outline-dark" data-toggle="modal" data-target="#addNew2">Додај новост</button>
                    </div>
                </div>
                <div class="row d-flex">
                    @if(count($news) == null)
                        <div class="col-12 text-center">
                            <h2>Нема резултата</h2>
                        </div>
                    @endif
                    @foreach($news as $new)
                        <div class="col-12 col-md-4 justify-content-center mb-4">
                            <div class="card custom-card" style="width: 18rem;">
                                <div class="image-container text-center">
                                    <img class="card-img-top" src="{{asset('uploads/'.$new->path)}}">
                                </div>
                                <div class="card-body">
                                    <h5 class="card-title">{{str_limit($new->name,60)}}</h5>
                                    <p class="card-text grey-font font-14">{{$new->type}}</p>
                                    <form action="{{asset('admin/delete_news/'.$new->id)}}" method="GET">
                                        <button data-toggle="modal" data-target="#editOsNews{{$new->id}}" type="button" class="btn btn-outline-info">Измени</button>
                                        <button type="submit" class="btn btn-outline-danger">Обриши</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- edit news -->
                        <div class="modal fade" id="editOsNews{{$new->id}}" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <form action="news/{{$new->id}}" method="POST" enctype="multipart/form-data">
                                        {!! csrf_field() !!}
                                        <input hidden value="OTHER" name="category">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Измени новост</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body mt-3">
                                            <input type="file" name="avatar" accept="image/x-png,image/gif,image/jpeg"/><br>
                                            <img style="width: 200px;" src="{{asset('uploads/'.$new->path)}}">
                                            <div class="form-group mt-2">
                                                <input type="text" class="form-control" value="{{$new->name}}" placeholder="Наслов" name="name" required>
                                            </div>
                                            <div class="form-group">
                                                <select class="form-control" name="type" required>
                                                    <option value="" disabled hidden selected>Врста новости</option>
                                                    <option @if($new->type == 'НАЈВАЖНИЈЕ'){{'selected'}}@endif>НАЈВАЖНИЈЕ</option>
                                                    <option @if($new->type == 'КОНФЕРЕНЦИЈА'){{'selected'}}@endif>КОНФЕРЕНЦИЈА</option>
                                                    <option @if($new->type == 'ИЗВЕШТАЈ'){{'selected'}}@endif>ИЗВЕШТАЈ</option>
                                                </select>
                                            </div>
                                            {{--TINYMCE!!!!!--}}
                                            <div class="form-group">
                                                <textarea class="edit-desc-tiny" name="desc" rows="10" placeholder="Опис" required >{{$new->desc}}</textarea>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-danger" onclick="checkBeforeUpdate()">Сачувај</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="admin-pagination">
                    {{ $news->links('vendor.pagination.bootstrap-4') }}
                </div>
                <!-- add new -->
                <div class="modal fade" id="addNew2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <form action="news" method="POST" enctype="multipart/form-data">
                                {!! csrf_field() !!}
                                <input hidden value="OTHER" name="category">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Додај новост</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body mt-3">
                                    <input type="file" name="avatar" accept="image/x-png,image/gif,image/jpeg" required/>
                                    <div class="form-group mt-2">
                                        <input type="text" class="form-control" placeholder="Наслов" name="name" required>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control" name="type" required>
                                            <option value="" disabled hidden selected>Врста новости</option>
                                            <option>НАЈВАЖНИЈЕ</option>
                                            <option>КОНФЕРЕНЦИЈА</option>
                                            <option>ИЗВЕШТАЈ</option>
                                        </select>
                                    </div>
                                    {{--TINYMCE!!!!!--}}
                                    <div class="form-group">
                                        <textarea id="desc-tiny" name="desc" rows="10" placeholder="Опис" required ></textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-danger" onclick="checkBeforeUpdate()">Сачувај</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane container fade" id="experts">
                {{--EXPERTS--}}
                <div class="row mt-5 mb-md-5 mb-5">
                    <div class="col-12 col-sm-8">
                        <h2>Чланови стручног штаба</h2>
                    </div>
                    <div class="col-12 col-sm-4 text-left text-sm-right">
                        <button type="button" class="btn btn-outline-dark" data-toggle="modal" data-target="#addNewExpert">Додај члана штаба</button>
                    </div>
                </div>
                <div class="row d-flex">
                    @if(count($experts) == null)
                        <div class="col-12 text-center">
                            <h2>Нема резултата</h2>
                        </div>
                    @endif
                    @foreach($experts as $expert)
                        <div class="col-12 col-md-4 justify-content-center mb-4">
                            <div class="card custom-card" style="width: 18rem;">
                                <div class="image-container text-center">
                                    <img class="card-img-top" src="{{asset('uploads/'.$expert->path)}}">
                                </div>
                                <div class="card-body">
                                    <h5 class="card-title">{{$expert->name}} {{$expert->lastname}}</h5>
                                    <p class="card-text grey-font font-14">{{$expert->position}}</p>
                                    <form action="{{asset('admin/delete_expert/'.$expert->id)}}" method="GET">
                                        <button data-toggle="modal" data-target="#editOsExpert{{$expert->id}}" type="button" class="btn btn-outline-info">Измени</button>
                                        <button type="submit" class="btn btn-outline-danger">Обриши</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- edit experts -->
                        <div class="modal fade" id="editOsExpert{{$expert->id}}" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <form action="expert/{{$expert->id}}" method="POST" enctype="multipart/form-data">
                                        {!! csrf_field() !!}
                                        <input hidden value="OTHER" name="category">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Измени члана штаба</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body mt-3">
                                            <input type="file" name="avatar" accept="image/x-png,image/gif,image/jpeg"/><br>
                                            <img style="width: 200px;" src="{{asset('uploads/'.$expert->path)}}">
                                            <div class="form-group mt-2">
                                                <input type="text" class="form-control" value="{{$expert->name}}" placeholder="Име" name="name" required>
                                            </div>
                                            <div class="form-group mt-2">
                                                <input type="text" class="form-control" value="{{$expert->lastname}}" placeholder="Презиме" name="lastname" required>
                                            </div>
                                            <div class="form-group mt-2">
                                                <input type="text" class="form-control" value="{{$expert->position}}" placeholder="Позиција" name="position" required>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-danger">Сачувај</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <!-- add new -->
            <div class="modal fade" id="addNewExpert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <form action="expert" method="POST" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <input hidden value="OTHER" name="category">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Додај члана штаба</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body mt-3">
                                <input type="file" name="avatar" accept="image/x-png,image/gif,image/jpeg" required/>
                                <div class="form-group mt-2">
                                    <input type="text" class="form-control" placeholder="Име" name="name" required>
                                </div>
                                <div class="form-group mt-2">
                                    <input type="text" class="form-control" placeholder="Презиме" name="lastname" required>
                                </div>
                                <div class="form-group mt-2">
                                    <input type="text" class="form-control" placeholder="Позиција" name="position" required>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger">Сачувај</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @foreach($selections as $selection)
            <div class="tab-pane container fade" id="selection{{$selection->id}}">
                <div class="row mt-4">
                    <div class="col-12 col-sm-8">
                        <h2>{{$selection->name}}</h2>
                    </div>
                    <div class="col-12 col-sm-4 text-left text-sm-right">
                        <form action="{{asset('admin/delete_second_team/'.$selection->id)}}" method="GET">
                            <button type="submit" class="btn btn-outline-dark">Обриши селекцију</button>
                        </form>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-12">
                        <form action="second-teams/{{$selection->id}}" method="POST" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <div>
                                <input type="file" name="avatar" accept="image/x-png,image/gif,image/jpeg"/><br>
                                <img style="width: 400px;" src="{{asset('uploads/'.$selection->path)}}">
                            </div>
                            <div class="form-group mt-2">
                                <input type="text" class="form-control" value="{{$selection->name}}" placeholder="Име селекције" name="name" required>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="players" rows="10" placeholder="Чланови селекције" required>{{$selection->players}}</textarea>
                            </div>
                            <div class="form-group">
                                <textarea class="matches-tiny" name="matches" rows="10" placeholder="Резултати" required>{{$selection->matches}}</textarea>
                            </div>
                            <button type="submit" onclick="checkBeforeUpdate()" class="btn btn-danger">Сачувај измене</button>
                        </form>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="tab-pane container fade" id="new-selection">
                <div class="row mt-4">
                    <div class="col-12">
                        <h2>Додај нову селекцију</h2>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-12">
                        <form action="second-teams" method="POST" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <input type="file" name="avatar" accept="image/x-png,image/gif,image/jpeg" required/>
                            <div class="form-group mt-2">
                                <input type="text" class="form-control" placeholder="Име селекције" name="name" required>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="players" rows="10" placeholder="Чланови селекције" required></textarea>
                            </div>
                            <div class="form-group">
                                <textarea class="matches-tiny" name="matches" rows="10" placeholder="Резултати" required></textarea>
                            </div>
                            <button type="submit" class="btn btn-danger" onclick="checkBeforeUpdate()">Сачувај</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function checkBeforeUpdate() {
            tinyMCE.triggerSave();

            // Get element from '#desc-tiny' and check value if empty make focus on '#desc-tiny_ifr'
        }
    </script>
@endsection
