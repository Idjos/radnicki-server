@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mb-md-5 mb-5">
            <div class="col-12 col-sm-8">
                <h2>Чланови стручног штаба</h2>
            </div>
            <div class="col-12 col-sm-4 text-left text-sm-right">
                <button type="button" class="btn btn-outline-dark" data-toggle="modal" data-target="#addNew">Додај члана штаба</button>
            </div>
        </div>
        <div class="row d-flex">
            @if(count($experts) == null)
                <div class="col-12 text-center">
                    <h2>Нема резултата</h2>
                </div>
            @endif
            @foreach($experts as $expert)
                <div class="col-12 col-md-4 justify-content-center mb-4">
                    <div class="card custom-card" style="width: 18rem;">
                        <div class="image-container text-center">
                            <img class="card-img-top" src="{{asset('uploads/'.$expert->path)}}">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">{{$expert->name}} {{$expert->lastname}}</h5>
                            <p class="card-text grey-font font-14">{{$expert->position}}</p>
                            <form action="{{asset('admin/delete_expert/'.$expert->id)}}" method="GET">
                                <button data-toggle="modal" data-target="#editExpert{{$expert->id}}" type="button" class="btn btn-outline-info">Измени</button>
                                <button type="submit" class="btn btn-outline-danger">Обриши</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- edit experts -->
                <div class="modal fade" id="editExpert{{$expert->id}}" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <form action="expert/{{$expert->id}}" method="POST" enctype="multipart/form-data">
                                {!! csrf_field() !!}
                                <input hidden value="FIRST-TEAM" name="category">
                                <div class="modal-header">
                                    <h5 class="modal-title">Измени члана штаба</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body mt-3">
                                    <input type="file" name="avatar" accept="image/x-png,image/gif,image/jpeg"/><br>
                                    <img style="width: 200px;" src="{{asset('uploads/'.$expert->path)}}">
                                    <div class="form-group mt-2">
                                        <input type="text" class="form-control" value="{{$expert->name}}" placeholder="Име" name="name" required>
                                    </div>
                                    <div class="form-group mt-2">
                                        <input type="text" class="form-control" placeholder="Презиме" value="{{$expert->lastname}}" name="lastname" required>
                                    </div>
                                    <div class="form-group mt-2">
                                        <input type="text" class="form-control" placeholder="Позиција" value="{{$expert->position}}" name="position" required>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="edit-desc-tiny" name="desc" rows="10" placeholder="Опис" required>{{$expert->desc}}</textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-danger" onclick="checkBeforeUpdate()">Сачувај</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <!-- add new -->
    <div class="modal fade" id="addNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="expert" method="POST" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <input hidden value="FIRST-TEAM" name="category">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Додај члана штаба</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mt-3">
                        <input type="file" name="avatar" accept="image/x-png,image/gif,image/jpeg" required/>

                        <div class="form-group mt-2">
                            <input type="text" class="form-control" placeholder="Име" name="name" required>
                        </div>
                        <div class="form-group mt-2">
                            <input type="text" class="form-control" placeholder="Презиме" name="lastname" required>
                        </div>
                        <div class="form-group mt-2">
                            <input type="text" class="form-control" placeholder="Позиција" name="position" required>
                        </div>
                        <div class="form-group">
                            <textarea class="edit-desc-tiny" name="desc" rows="10" placeholder="Опис" required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger" onclick="checkBeforeUpdate()">Сачувај</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        function checkBeforeUpdate() {
            tinyMCE.triggerSave();

            // Get element from '#desc-tiny' and check value if empty make focus on '#desc-tiny_ifr'
        }

        $(function() {

            var bar = $('.bar');
            var percent = $('.percent');
            var status = $('#status');

            $('form').ajaxForm({
                beforeSend: function() {
                    status.empty();
                    var percentVal = '0%';
                    bar.width(percentVal);
                    percent.html(percentVal);
                },
                uploadProgress: function(event, position, total, percentComplete) {
                    var percentVal = percentComplete + '%';
                    bar.width(percentVal);
                    percent.html(percentVal);
                },
                complete: function(xhr) {
                    status.html(xhr.responseText);
                }
            });
        });
    </script>
@endsection
