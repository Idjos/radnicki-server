@extends('layouts.app')
{{--dogadjaji--}}
@section('content')
    {{--    {{dd($events)}}--}}
    <div class="container">
        <div class="row mb-md-5 mb-5">
            <div class="col-12">
                <h2>Табела</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <form method="POST" action="{{asset('admin/table')}}">
                    {!! csrf_field() !!}
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th scope="col">Позиција</th>
                            <th scope="col">Име тима</th>
                            <th scope="col">Број одиграних мечева</th>
                            <th scope="col">Број бодова</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($teams as $key => $team)
                            <input hidden name="table[{{$key}}][id]" value="{{$team->id}}">
                            <tr>
                                <th class="text-center" scope="row">{{$key + 1}}</th>
                                <td>
                                    <div class="form-group">
                                        <input type="text" name="table[{{$key}}][name]" value="{{$team->name}}" class="form-control" placeholder="Име тима" required>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" name="table[{{$key}}][played_games]" value="{{$team->played_games}}" class="form-control" placeholder="Број мечева" required>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" name="table[{{$key}}][points]" value="{{$team->points}}" class="form-control" placeholder="Број бодова" required>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="text-right">
                        <button type="submit" class="btn btn-danger mb-5">Сачувај</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
