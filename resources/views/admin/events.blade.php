@extends('layouts.app')
{{--dogadjaji--}}
@section('content')
{{--    {{dd($events)}}--}}
    <div class="container">
        <div class="row mb-md-5 mb-5">
            <div class="col-12 col-sm-8">
                <h2>Календар</h2>
            </div>
            <div class="col-12 col-sm-4 text-left text-sm-right">
                <button type="button" class="btn btn-outline-dark" data-toggle="modal" data-target="#addNew">Додај догађај</button>
            </div>
        </div>
        <div class="row d-flex">
            @if(count($events) == null)
                <div class="col-12 text-center">
                    <h2>Нема резултата</h2>
                </div>
            @endif
            @foreach($events as $event)
                <div class="col-12 col-md-4 justify-content-center mb-4">
                    <div class="card custom-card" style="width: 18rem; height: 250px">
                        <div class="card-body">
                            <h5 class="card-title">{{str_limit($event->name, 60)}}</h5>
                            <p class="card-text grey-font font-14">{{str_limit($event->desc, 180)}}</p>
                            <form action="{{asset('admin/delete_events/'.$event->id)}}" method="GET">
                                <button data-toggle="modal" data-target="#editEvent{{$event->id}}" type="button" class="btn btn-outline-info">Измени</button>
                                <button type="submit" class="btn btn-outline-danger">Обриши</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- edit event -->
                <div class="modal fade" id="editEvent{{$event->id}}" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <form action="events/{{$event->id}}" method="POST">
                                {!! csrf_field() !!}
                                <input hidden value="FIRST-TEAM" name="category">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Измени догађај</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body mt-3">
                                    <div class="form-group mt-2">
                                        <input type="text" class="form-control" placeholder="Наслов" name="name" value="{{$event->name}}" required>
                                    </div>
                                    <div class="form-group mt-2">
                                        <input type="date" class="form-control" placeholder="Датум" name="date" value="{{$event->date}}" required>
                                    </div>
                                    <div class="form-group mt-2">
                                        <input type="text" class="form-control" placeholder="Опис" name="desc" value="{{$event->desc}}" required>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-danger">Сачувај</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="admin-pagination">
            {{ $events->links('vendor.pagination.bootstrap-4') }}
        </div>
    </div>

    <!-- add new -->
    <div class="modal fade" id="addNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="events" method="POST">
                    {!! csrf_field() !!}
                    <input hidden value="FIRST-TEAM" name="category">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Додај догађај</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mt-3">
                        <div class="form-group mt-2">
                            <input type="text" class="form-control" placeholder="Наслов" name="name" required>
                        </div>
                        <div class="form-group mt-2">
                            <input type="date" class="form-control" placeholder="Датум" name="date" required>
                        </div>
                        <div class="form-group mt-2">
                            <input type="text" class="form-control" placeholder="Опис" name="desc" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">Сачувај</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
