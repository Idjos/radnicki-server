@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mb-md-5 mb-5">
            <div class="col-12 col-sm-8">
                <h2>Новости</h2>
            </div>
            <div class="col-12 col-sm-4 text-left text-sm-right">
                <button type="button" class="btn btn-outline-dark" data-toggle="modal" data-target="#addNew">Додај новост</button>
            </div>
        </div>
        <div class="row d-flex">
            @if(count($news) == null)
                <div class="col-12 text-center">
                    <h2>Нема резултата</h2>
                </div>
            @endif
            @foreach($news as $new)
            <div class="col-12 col-md-4 justify-content-center mb-4">
                <div class="card custom-card" style="width: 18rem;">
                    <div class="image-container text-center">
                        <img class="card-img-top" src="{{asset('uploads/'.$new->path)}}">
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">{{str_limit($new->name,60)}}</h5>
                        <p class="card-text grey-font font-14">{{$new->type}}</p>
                        <form action="{{asset('admin/delete_news/'.$new->id)}}" method="GET">
                            <button data-toggle="modal" data-target="#editNews{{$new->id}}" type="button" class="btn btn-outline-info">Измени</button>
                            <button type="submit" class="btn btn-outline-danger">Обриши</button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- edit news -->
            <div class="modal fade" id="editNews{{$new->id}}" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <form action="news/{{$new->id}}" method="POST" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <input hidden value="FIRST-TEAM" name="category">
                            <div class="modal-header">
                                <h5 class="modal-title">Измени новост</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body mt-3">
                                <input type="file" name="avatar" accept="image/x-png,image/gif,image/jpeg"/><br>
                                <img style="width: 200px;" src="{{asset('uploads/'.$new->path)}}">
                                <div class="form-group mt-2">
                                    <input type="text" class="form-control" value="{{$new->name}}" placeholder="Наслов" name="name" required>
                                </div>
                                <div class="form-group">
                                    <select class="form-control" name="type" required>
                                        <option value="" disabled hidden selected>Врста новости</option>
                                        <option @if($new->type == 'НАЈВАЖНИЈЕ'){{'selected'}}@endif>НАЈВАЖНИЈЕ</option>
                                        <option @if($new->type == 'КОНФЕРЕНЦИЈА'){{'selected'}}@endif>КОНФЕРЕНЦИЈА</option>
                                        <option @if($new->type == 'ИЗВЕШТАЈ'){{'selected'}}@endif>ИЗВЕШТАЈ</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <textarea class="edit-desc-tiny" name="desc" rows="10" placeholder="Опис" required>{{$new->desc}}</textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger" onclick="checkBeforeUpdate()">Сачувај</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="admin-pagination">
            {{ $news->links('vendor.pagination.bootstrap-4') }}
        </div>
    </div>

    <!-- add new -->
    <div class="modal fade" id="addNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="news" method="POST" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <input hidden value="FIRST-TEAM" name="category">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Додај новост</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mt-3">
                        <input type="file" name="avatar" accept="image/x-png,image/gif,image/jpeg" required/>
                        <div class="form-group mt-2">
                            <input type="text" class="form-control" placeholder="Наслов" name="name" required>
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="type" required>
                                <option value="" disabled hidden selected>Врста новости</option>
                                <option>НАЈВАЖНИЈЕ</option>
                                <option>КОНФЕРЕНЦИЈА</option>
                                <option>ИЗВЕШТАЈ</option>
                            </select>
                        </div>
                        {{--TINYMCE!!!!!--}}
                        <div class="form-group">
                            <textarea id="desc-tiny" name="desc" rows="10" placeholder="Опис" required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger" onclick="checkBeforeUpdate()">Сачувај</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        function checkBeforeUpdate() {
            tinyMCE.triggerSave();

            // Get element from '#desc-tiny' and check value if empty make focus on '#desc-tiny_ifr'
        }
    </script>
@endsection
